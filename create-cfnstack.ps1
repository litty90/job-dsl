﻿
Param(
    [Parameter(Mandatory)][string]$StackName,
    [Parameter(Mandatory)][int]$SecondOctet
)

function New-AWSParam
{
    Param([Parameter(Mandatory)][string]$Key, [Parameter(Mandatory)][string]$Value)
    $p1 = New-Object -Type Amazon.CloudFormation.Model.Parameter
    $p1.ParameterKey = $Key
    $p1.ParameterValue = $value
    return $p1
}

function New-AWSPeering
{
    $AccountId = '985921142170'
    New-EC2VpcPeeringConnection -PeerOwnerId $AccountId -VpcId vpc-bf6908da -PeerVpcId vpc-66bdd303
}

#copy template to s3
$TemplateFile = 'C:\Repos\gum\provision\aws\templates\Template_2_DSC_1AZ.template'
Write-s3object -BucketName cf-templates-h1dtcypi5icj-us-west-2 -key cb-Template_2_DSC_1AZ.template -File $TemplateFile -Force

$templateUrl = 'https://s3-us-west-2.amazonaws.com/cf-templates-h1dtcypi5icj-us-west-2/cb-Template_2_DSC_1AZ.template'
$x = $SecondOctet
$Parameters = @(
    (New-AWSParam  ADServer1PrivateIp "10.$x.0.10"),
    (New-AWSParam  ADServer2PrivateIp  "10.$x.64.10"),
    (New-AWSParam  DMZ1CIDR  "10.$x.32.0/20"),
    (New-AWSParam  DMZ2CIDR  "10.$x.96.0/20"),
    (New-AWSParam  DomainDNSName  "example$x.com"),
    (New-AWSParam  DomainNetBiosName  "EXAMPLE$x"),
    (New-AWSParam  KeyPairName  'cb-test1'),
    (New-AWSParam  NatServer1PrivateIp  "10.$x.0.20"),
    (New-AWSParam  NatServer1PublicIp  "10.$x.32.30"),
    (New-AWSParam  NatServer2PrivateIp  "10.$x.64.20"),
    (New-AWSParam  NatServer2PublicIp  "10.$x.96.30"),
    (New-AWSParam  PrivateSubnet1CIDR  "10.$x.0.0/24"),
    (New-AWSParam  PrivateSubnet2CIDR  "10.$x.64.0/19"),
    (New-AWSParam  PrivateSubnet3CIDR  "10.$x.128.0/19"),
    (New-AWSParam  VPCCIDR  "10.$x.0.0/16")
)

New-CFNStack -StackName $StackName -DisableRollback $true -Parameter $Parameters -TemplateURL $templateUrl
