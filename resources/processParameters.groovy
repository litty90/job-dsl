/**
 * Created by grady on 9/14/2015.
 */

import groovy.text.SimpleTemplateEngine;
import groovy.util.CliBuilder
import groovy.json.JsonSlurper
import groovy.json.StringEscapeUtils;

def engine = new groovy.text.SimpleTemplateEngine()

def cli = new CliBuilder()
cli.help('Print this message.')
cli.inputFile(args:1, argName:'inputFile', 'input xml file')
cli.outputFile(args:2, argName:'outputFile', 'output xml file')
cli.binding(args:3, argName:'binding', 'binding')

def options = cli.parse(this.args)

// escape '\'
String parameters = new File(options['inputFile']).text.replace('\\', '\\\\')
// escape '$'
parameters = parameters.replace('$', '\\$')
//  unescape '${' (dollar signs above we're escaped, thus breaking parameter substitution
parameters = parameters.replace('\\${', '${')

def binding = [:]
if(options.getProperty('binding')) {
    // kludge: hack to allow passing (") through jenkins groovy plugin
    def tmp = options['binding'].replace("\'", "\"")
    tmp = StringEscapeUtils.unescapeJavaScript(tmp)
    binding = new JsonSlurper().parseText(tmp);
}
def template = engine.createTemplate(parameters).make(binding);

// open output file - delete if it exists
def f = new File(options['outputFile'])

if(f.exists()) {
    f.delete()
}
f.createNewFile()

// write template
f << template
