import com.alertsense.MSBuildCiJobBuilder
import com.alertsense.FluentMigratorCiJobBuilder
import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'aspen'
def gitRepo = 'aspen'
def workspace = 'Y:\\Jenkins\\workspace\\aspen'
def solutionDirectory = "$workspace"
def solutionFile = 'AlertSense.Aspen.sln'

// db
def connection = "Data Source=${dbServer},1433;Database=aspen;Integrated Security=True"
def dbAssembly = "${solutionDirectory}\\AlertSense.Aspen.Data\\bin\\Debug\\AlertSense.Aspen.Data.dll"
def profiles = []

// web
def publishProfile = 'production'
def siteName = 'aspen.alertsense.com'
def projectDirectory = "${solutionDirectory}\\AlertSense.Aspen"
def projectFile = "AlertSense.Aspen.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}


def jobName ="$stack/$basePath/create"
def nextJob = "$stack/$basePath/migrate"
new MSBuildCiJobBuilder(
    name: jobName,
    description: "Build and package ${basePath}.",
    workspace: workspace,
    ownerAndProject: "git@bitbucket.org:AlertSense/${gitRepo}.git",
    gitBranch: gitBranch,
    solutionDirectory: solutionDirectory,
    solutionFile: solutionFile,
    downStreamPublisher: nextJob
).build(this)


jobName = nextJob
nextJob = "$stack/$basePath/deploy"
new FluentMigratorCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connection,
        assembly: dbAssembly,
        profiles: profiles,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = "$stack/$basePath/migrate-instance"
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        paramtersBinding: parameters,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)