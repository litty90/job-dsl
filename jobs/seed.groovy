// If you want, you can define your seed job in the DSL and create it via the REST API.
// See https://github.com/sheehan/job-dsl-gradle-example#rest-api-runner
import hudson.model.*
import jenkins.model.*

import java.util.logging.Logger

def logger = Logger.getLogger("")

def repositoryUrl = "git@bitbucket.org:AlertSense/job-dsl.git"
def branchName = "slave"

logger.info("Creating aws-seed job.")

job('aws-seed') {
    label 'master'
    scm {
        git {
            remote {
                url(repositoryUrl)
            }
            branch(branchName)
            clean(true)
        }
    }
    steps {
        dsl {
            external 'jobs/alertsense/*.groovy'
            additionalClasspath 'src/main/groovy'
        }
    }
}

job('code-review-seed') {
    label 'master'
    scm {
        git {
            remote {
                url(repositoryUrl)
            }
            branch(branchName)
            clean(true)
        }
    }
    steps {
        dsl {
            external 'jobs/*Pipeline.groovy'
            external 'jobs/admin.groovy'
            additionalClasspath 'src/main/groovy'
        }
    }
}