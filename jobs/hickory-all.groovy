import com.alertsense.config.Configuration
import com.alertsense.MSBuildCiJobBuilder
import com.alertsense.FluentMigratorCiJobBuilder
import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//

def hickoryConfig = new Configuration()

hickoryConfig.name = "hickory"
hickoryConfig.repoName = "hickory"
hickoryConfig.solutionDirectory = "$workspace\\src"
hickoryConfig.solutionFile =  'AlertSense.Hickory.sln'

def basePath = 'hickory'
def gitRepo = 'hickory'
def workspace = 'Y:\\Jenkins\\workspace\\hickory'
def solutionDirectory = "$workspace\\src"
def solutionFile = 'AlertSense.Hickory.sln'

// db
def connection = "Data Source=${dbServer},1433;Database=prod-hickory;Integrated Security=True"
def dbAssembly = "${solutionDirectory}\\DAL\\AlertSense.Hickory.Data.Setup\\bin\\Debug\\AlertSense.Hickory.Data.Setup.dll"
def profiles = ['static', 'DouglasR911Test', 'EasDevelopment']

// web
def publishProfile = 'prod-admin'
def siteName = 'hickory.alertsense.com'
def projectDirectory = "${solutionDirectory}\\App\\AlertSense.Hickory.AdminWebsite"
def projectFile = "AlertSense.Hickory.AdminWebsite.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}


def jobName ="$stack/$basePath/create"
def nextJob = "$stack/$basePath/migrate"
new MSBuildCiJobBuilder(
    name: jobName,
    description: "Build and package ${basePath}.",
    workspace: workspace,
    ownerAndProject: "git@bitbucket.org:AlertSense/${gitRepo}.git",
    gitBranch: gitBranch,
    solutionDirectory: solutionDirectory,
    solutionFile: solutionFile,
    downStreamPublisher: nextJob
).build(this)


jobName = nextJob
nextJob = "$stack/$basePath/deploy"
new FluentMigratorCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connection,
        assembly: dbAssembly,
        profiles: profiles,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = "$stack/$basePath/deploy-public"
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        paramtersBinding: parameters,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)