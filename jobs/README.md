# H1  README

#H2 How to add a pipe

copy the projectPipeline.groovy.template file and change all the $XXX to
be the correct information.

$PROJECT_NAME : typically the repo name, or common name of the project (elm, hickory)

$GIT_REPO_URL : The bitbucket git clone URL

$GERRIT_REPO_URL : The gerrit URL.  Be convention, the url is ssh://jenkins@gerrit.dev.aws.alertsense.net:29418/<project name>

$REVIEW_CMD : The command to be run for the gerrit-review job.  Typically does a full build and runs the unit tests.

$INTEGRATE_CMD:  The command used to verify correctness of the integration.  Usually the same as the above.

$DEPLOY_CMD :  The command to deploy the system.

# H2  How to recreate the job dsl

1.  Create a local jenkins.
2.  Create a job plugin job.
3.  Build the job-dsl locally
4.  Verify correctness

5.  After verifying correct behavior, push the git and do a system wide job build.

