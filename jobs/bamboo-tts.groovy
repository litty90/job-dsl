import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'bamboo'
def workspace = 'Y:\\Jenkins\\workspace\\bamboo'
def solutionDirectory = "$workspace\\src"

// web
def publishProfile = 'prod-tts'
def siteName = 'tts.alertsense.com'
def projectDirectory = "${solutionDirectory}\\Applications\\AlertSense.Web.TtsService"
def projectFile = "AlertSense.Web.TtsService.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
        description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
        description "CI jobs for ${stack}/${basePath}"
}

def jobName ="$stack/$basePath/deploy-tts"
def nextJob = null
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        paramtersBinding: parameters,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)