import com.alertsense.BuildCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'hickory'
def gitRepo = 'hickory'
def workspace = 'Y:\\Jenkins\\workspace\\hickory'
def solutionDirectory = "$workspace\\src"
def solutionFile = 'AlertSense.Hickory.sln'

// db
def connection = "Data Source=${dbServer},1433;Database=prod-hickory;Integrated Security=True"
def dbAssembly = "${solutionDirectory}\\DAL\\AlertSense.Hickory.Data.Setup\\bin\\Debug\\AlertSense.Hickory.Data.Setup.dll"
def profiles = ['static', 'DouglasR911Test', 'EasDevelopment']

// web
def publishProfile = 'prod-admin'
def siteName = 'hickory.alertsense.com'
def projectDirectory = "${solutionDirectory}\\App\\AlertSense.Hickory.AdminWebsite"
def projectFile = "AlertSense.Hickory.AdminWebsite.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}




new BuildCiJobBuilder(
        gitBranch: "dev",
        config: hickoryConfiguration
).build(this)
