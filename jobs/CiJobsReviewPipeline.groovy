import Factory.CiJobFactory
import Factory.Pipeline


def Pipeline elmPipeline = new Pipeline([
        name        : "elm",

        originUrl: "git@bitbucket.com:AlertSense/elm-this-is-wrong",
        gerritUrl: "ssh://gerrit@bitbucket.com/elm-this-is-wrong",

        reviewCmd   : "echo review done",
        integrateCmd: "echo integrate done",
        deployCmd   : "echo deploy done"

])

def Pipeline hickoryPipeline = new Pipeline([
        name        : "hickory",

        originUrl: "git@bitbucket.com:AlertSense/hickory-this-is-wrong",
        gerritUrl: "ssh://gerrit@bitbucket.com/hickory-this-is-wrong",

        reviewCmd   : "echo review done",
        integrateCmd: "echo integrate done",
        deployCmd   : "echo deploy done"

])

def Pipeline cidemoPipeline = new Pipeline([
        name        : "cidemo",

        originUrl: "git@bitbucket.org:AlertSense/ci-demo",
        gerritUrl: "ssh://jenkins@gerrit.dev.aws.alertsense.net:29418/cidemo",

        reviewCmd   : ".\\hello.bat",
        integrateCmd: ".\\hello.bat",
        deployCmd   : ".\\hello.bat"

])


def Pipeline[] pipes = [elmPipeline, hickoryPipeline, cidemoPipeline]

// three branches
// master     :  currently being developed
// staging    :  currently under final QA before production
// production :  currently in production

def String[] branches = ["master", "staging", "production"]

def pipelineBuilder(String[] branches, Pipeline[] pipes) {

    factory = new CiJobFactory();

    // create the pipes
    branches.each {
        def String branch = it;
        pipes.each {
            def Pipeline pipe = it;
            pipe.job = pipe.name + "-" + branch
            pipe.branch = branch
            factory.reviewJob(this, pipe, pipe.reviewCmd)
            factory.integrateJob(this, pipe, pipe.integrateCmd)
            factory.deployJob(this, pipe, pipe.deployCmd)
            factory.resetPipeJob(this, pipe)
        }
    }
    // create the views
    factory.jobViews(this, branches, pipes)

}

pipelineBuilder(branches, pipes)

