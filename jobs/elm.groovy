import com.alertsense.MSBuildCiJobBuilder
import com.alertsense.FluentMigratorCiJobBuilder
import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'elm'
def gitRepo = 'elm'
def workspace = 'Y:\\Jenkins\\workspace\\elm'
def solutionDirectory = "$workspace\\src"
def solutionFile = 'AlertSense.Elm.sln'

// db
def connection = "Data Source=${dbServer},1433;Database=prod-elm;Integrated Security=True"
def dbAssembly = "${solutionDirectory}\\DAL\\AlertSense.Elm.Data.Setup\\bin\\Debug\\AlertSense.Elm.Data.Setup.dll"
def profiles = ['DevelopmentElm']

// web
def publishProfile = 'prod-elm'
def siteName = 'elm.alertsense.com'
def projectDirectory = "${solutionDirectory}\\App\\AlertSense.Elm.MappingWebsite"
def projectFile = "AlertSense.Elm.MappingWebsite.csproj"

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}


def jobName ="$stack/$basePath/create"
def nextJob = "$stack/$basePath/migrate"
new MSBuildCiJobBuilder(
    name: jobName,
    description: "Build and package ${basePath}.",
    workspace: workspace,
    ownerAndProject: "git@bitbucket.org:AlertSense/${gitRepo}.git",
    gitBranch: gitBranch,
    solutionDirectory: solutionDirectory,
    solutionFile: solutionFile,
    downStreamPublisher: nextJob
).build(this)


jobName = nextJob
nextJob = "$stack/$basePath/deploy"
new FluentMigratorCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connection,
        assembly: dbAssembly,
        profiles: profiles,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = "$stack/$basePath/deploy-public"
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)