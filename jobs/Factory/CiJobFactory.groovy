package Factory

public class CiJobFactory {

    def reviewJob(dslFactory, Pipeline pipe, String cmd) {
        def String jobName = pipe.job + "-gerrit-review"

        dslFactory.job(jobName) {
            description '''Gerrit Review job, created by job-dsl do not hand edit'''
            logRotator {
                daysToKeep 28
            }

            label("windows-jenkins-slave")
            triggers {
                gerrit {
                    events {
                        patchsetCreated()
                    }
                    project(pipe.job, pipe.branch)
                }
            }

            scm {
                git {
                    remote {
                        name("gerrit")
                        url(pipe.gerritUrl + "-" + pipe.branch)
                        refspec("\$GERRIT_REFSPEC")
                    }
                    branch(pipe.branch)
                    strategy {
                        gerritTrigger()
                    }
                }
            }
            steps {
                batchFile(cmd)
            }
            publishers {
                extendedEmail(null) {
                    trigger('Always')
                    trigger(triggerName: 'Failure',
                            subject: 'BUILD IS BROKEN = ' + jobName + ', and you might have caused it...',
                            sendToDevelopers: true,
                            includeCulprits: true)
                }
            }
        }
    }

    def integrateJob(dslFactory, Pipeline pipe, String cmd) {
        def jobName = pipe.job + "-integrate"

        dslFactory.job(jobName) {

            description '''Integration job, created by job-dsl do not hand edit'''
            logRotator {
                daysToKeep 28
            }
            label 'windows-jenkins-slave'
            triggers {
                scm("* * * * *")
            }
            scm {
                git {
                    remote {
                        name("origin")
                        url(pipe.originUrl)
                        credentials("7965899b-376f-4645-9639-a893315840a5")
                    }
                    remote {
                        name("gerrit")
                        url(pipe.gerritUrl + "-" + pipe.branch)
                    }
                    branch("origin/iq/" + pipe.branch + "/**")
                }
            }

            steps {
                batchFile '''
                        rd /S /Q cibuild
                        git clone git@bitbucket.org:AlertSense/cibuild.git
                        '''.stripIndent().trim()
                batchFile '''"c:\\Program Files\\Git\\bin\\bash" cibuild/iqmerge.sh ''' + pipe.branch
                batchFile cmd
            }

            publishers {
                postBuildScripts {
                    steps {
                        batchFile("cibuild/iqpostbuild.bat")
                    }
                    onlyIfBuildSucceeds(false)
                }
                postBuildScripts {
                    steps {
                        batchFile("cibuild/iqpostbuildsuccess.bat")
                    }
                    onlyIfBuildSucceeds(true)
                }
                extendedEmail(null) {
                    trigger('Always')
                    trigger(triggerName: 'Failure',
                            subject: 'INTEGRATION BUILD = ' + jobName + ' IS BROKEN, and you caused it...',
                            sendToDevelopers: true,
                            includeCulprits: true)
                }
            }
        }
    }

    def deployJob(dslFactory, Pipeline pipe, String cmd) {
        def jobName = pipe.job + "-deploy"
        dslFactory.job(jobName) {
            description '''Deploy job, created by job-dsl do not hand edit'''

            label("windows-jenkins-slave")
            scm {
                git(pipe.originUrl, pipe.branch)
            }
            steps {
                batchFile(cmd)
            }
            publishers {
                extendedEmail(null) {
                    trigger('Always')
                    trigger(triggerName: 'Failure',
                            subject: 'DEPLOY BUILD = ' + jobName + ' IS BROKEN, and you caused it...',
                            sendToDevelopers: true,
                            includeCulprits: true)
                }
            }
        }
    }

    def promotePipeJob(dslFactory, Pipeline pipe) {
        def jobName = pipe.job + "-promote-pipe"
        def String cmd = "cibuild/promote-pipe.sh " + pipe.name + " " + pipe.branch

        dslFactory.job(jobName) {
            parameters {
                stringParam('GIT_REF_BASE', pipe.branch,
                        'The git reference (tag, branch, commit hash) to reset the pipe base revision.  Please take care...')
            }
            scm {
                git {
                    remote {
                        name("origin")
                        url(pipe.originUrl)
                        credentials("7965899b-376f-4645-9639-a893315840a5")
                    }
                    remote {
                        name("gerrit")
                        url(pipe.gerritUrl + "-" + pipe.branch)
                    }
                    branch(pipe.branch)
                }
            }
            steps {
                shell "rm -rf cibuild && git clone git@bitbucket.org:AlertSense/cibuild.git"
                shell cmd
            }
        }
    }

    def pipelineBuilder(dslFactory, String[] branches, Pipeline pipe) {

        // build the pipes
        branches.each {
            def String branch = it;
            pipe.job = pipe.name + "-" + branch
            pipe.branch = branch
            reviewJob(dslFactory, pipe, pipe.reviewCmd)
            integrateJob(dslFactory, pipe, pipe.integrateCmd)
            deployJob(dslFactory, pipe, pipe.deployCmd)
            promotePipeJob(dslFactory, pipe)

        }
        // build the views
        jobViews(dslFactory, branches, pipe)

    }

    def jobViews(dslFactory, String[] branches, Pipeline pipe) {

        dslFactory.nestedView(pipe.name + " pipes") {
            views {
                for (String branch : branches) {
                    def vname = pipe.name + "-" + branch

                    listView(vname + " pipe") {
                        jobs {
                            regex(vname + ".*")
                        }
                        columns {
                            status()
                            weather()
                            name()
                            lastSuccess()
                            lastFailure()
                            lastDuration()
                            buildButton()
                        }
                    }
                }
            }
        }
    }
}

	     

