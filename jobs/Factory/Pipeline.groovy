package Factory


public class Pipeline {
    public String name;          // pipeline name, usually the repo name
    public String job;           // don't set this...

    public String originUrl;     // origin git URL

    public String gerritUrl;     // gerrit git URL

    public String branch;        // do not set this...

    public String reviewCmd;     // the command string for the review job
    public String integrateCmd;  // the command string for the integrate job
    public String deployCmd;     // the command string for the deploy job

}
