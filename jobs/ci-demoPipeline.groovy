import Factory.CiJobFactory
import Factory.Pipeline

def Pipeline pipe = new Pipeline([
        name       : 'ci-demo',

        originUrl: 'git@bitbucket.org:AlertSense/ci-demo.git',
        gerritUrl: 'ssh://jenkins@gerrit-test.dev.aws.alertsense.net:29418' + '/' + 'ci-demo',

        reviewCmd   : '',
        integrateCmd: '',
        deployCmd   : ''

])

def String[] branches = [  'dev', ]

(new CiJobFactory()).pipelineBuilder(this, branches,pipe)