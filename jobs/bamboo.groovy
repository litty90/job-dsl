import com.alertsense.MSBuildCiJobBuilder
import com.alertsense.EntityFrameworkCiJobBuilder
import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'bamboo'
def gitRepo = 'bamboo'
def workspace = 'Y:\\Jenkins\\workspace\\bamboo'
def solutionDirectory = "$workspace\\src"
def solutionFile = 'AlertSense CAP.sln'

// db
def connection = "Data Source=${dbServer},1433;Database=AlertSenseCap_Generated;Integrated Security=True;MultipleActiveResultSets=true;User Instance=false"
def dbAssemblyDirectory = "${solutionDirectory}\\Libraries\\AlertSense.Core\\bin\\Debug"
def dbAssembly = "AlertSense.Core"

// web
def publishProfile = 'prod-cap'
def siteName = 'bamboo.alertsense.com'
def projectDirectory = "${solutionDirectory}\\Applications\\AlertSense.Website"
def projectFile = "AlertSense.Website.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}


def jobName ="$stack/$basePath/create"
def nextJob = "$stack/$basePath/migrate"
new MSBuildCiJobBuilder(
    name: jobName,
    description: "Build and package ${basePath}.",
    workspace: workspace,
    ownerAndProject: "git@bitbucket.org:AlertSense/${gitRepo}.git",
    gitBranch: gitBranch,
    solutionDirectory: solutionDirectory,
    solutionFile: solutionFile,
    downStreamPublisher: nextJob
).build(this)


jobName = nextJob
nextJob = "$stack/$basePath/deploy"
new EntityFrameworkCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connection,
        assemblyDirectory: dbAssemblyDirectory,
        assembly: dbAssembly,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = "$stack/$basePath/deploy-tts"
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        paramtersBinding: parameters,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)