def env = System.getenv()
def awsDomain = "${AWS_DOMAIN}"
def workspace = "C:\\Jenkins\\workspace\\gum"
def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def gitbranch = 'dev'
def basefolder = awsDomain

def projects = ['hickory','elm','aspen','bamboo','bark','pergo']

job("${basefolder}/buildjob-hickory") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-hickory-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
    publishers {
        downstreamParameterized {
            trigger ("buildjob-elm" , 'SUCCESS') { currentBuild()}
        }
    }
}
job("${basefolder}/buildjob-elm") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-elm-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
    publishers {
        downstreamParameterized {
            trigger ("buildjob-aspen" , 'SUCCESS') { currentBuild()}
        }
    }
}
job("${basefolder}/buildjob-aspen") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-aspen-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
    publishers {
        downstreamParameterized {
            trigger ("buildjob-bamboo" , 'SUCCESS') { currentBuild()}
        }
    }
}
job("${basefolder}/buildjob-bamboo") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-bamboo-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
    publishers {
        downstreamParameterized {
            trigger ("buildjob-bark" , 'SUCCESS') { currentBuild()}
        }
    }
}
job("${basefolder}/buildjob-bark") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-bark-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
    publishers {
        downstreamParameterized {
            trigger ("buildjob-pergo" , 'SUCCESS') { currentBuild()}
        }
    }
}
job("${basefolder}/buildjob-pergo") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    parameters {stringParam('AWS_DOMAIN','example1.com')}
    steps {
        dsl {
            external("provision/aws/deployment/aws-pergo-dsl.groovy")
            lookupStrategy('SEED_JOB')
        }
    }
}
