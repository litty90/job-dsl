import com.alertsense.FluentMigratorCiJobBuilder
import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'aspen'
def workspace = 'Y:\\Jenkins\\workspace\\aspen'
def solutionDirectory = "$workspace"

// db
def connection = "Data Source=${dbServer},1433;Database=Production-WorkflowInstanceStore;Integrated Security=True"
def dbAssembly = "${solutionDirectory}\\AlertSense.Aspen.Workflow.Data.Instance\\bin\\Debug\\AlertSense.Aspen.Workflow.Data.Instance.dll"
def connectionMonitoring = "Data Source=${dbServer},1433;Database=aspen-monitoring;Integrated Security=True"
def dbAssemblyMonitoring = "${solutionDirectory}\\AlertSense.Aspen.Workflow.Data.Tracking\\bin\\Debug\\AlertSense.Aspen.Workflow.Data.Tracking.dll"
def profiles = null

// web
def publishProfile = 'Production'
def siteName = 'aspen-workflow.alertsense.com'
def projectDirectory = "${solutionDirectory}\\AlertSense.Aspen.Workflow"
def projectFile = "AlertSense.Aspen.Workflow.csproj"

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
    description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
    description "CI jobs for ${stack}/${basePath}"
}

def jobName = "$stack/$basePath/migrate-instance"
def nextJob = "$stack/$basePath/migrate-monitoring"
new FluentMigratorCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connection,
        assembly: dbAssembly,
        profiles: profiles,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = "$stack/$basePath/deploy-workflow"
new FluentMigratorCiJobBuilder(
        name: jobName,
        description: "Migrates ${basePath}.",
        workspace: workspace,
        solutionDirectory: solutionDirectory,
        connection: connectionMonitoring,
        assembly: dbAssemblyMonitoring,
        profiles: profiles,
        downStreamPublisher:  nextJob
).build(this)

jobName = nextJob
nextJob = null
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)