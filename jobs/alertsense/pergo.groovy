
package alertsense

import com.alertsense.config.BranchConfig
import com.alertsense.config.Configuration
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.DatabaseConfiguration
import com.alertsense.config.EnvironmentType
import com.alertsense.config.ProjectConfiguration
import com.alertsense.builder.ProjectPipelineBuilder
import com.alertsense.config.StackConfig
import com.alertsense.config.WebsiteConfiguration


def config = new Configuration(
        name: "pergo",
        repoPath: ".",
        baseDirectory: ".\\src",
        solutionFile: "AlertSense.Pergo.sln",
        databaseName: "pergo",
        websiteName: "pergo.alertsense.com",
        gitRepository: "git@bitbucket.org:AlertSense/Pergo.git",
        gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
        nugetConfig: ConfigurationFactory.getNugetConfig(),
        buildProjects: [
                "AlertSense.Pergo" : new ProjectConfiguration(
                        projectName: "AlertSense.Pergo",
                        projectFile: "AlertSense.Pergo.csproj",
                        projectFolder: ".\\src\\AlertSense.Pergo",
                ),
                "AlertSense.Pergo.Data": new ProjectConfiguration(
                        projectFile: "AlertSense.Pergo.Data.csproj",
                        projectFolder: ".\\src\\AlertSense.Pergo.Data",
                        publishNuget: true
                )
        ],
        databases: [
                new DatabaseConfiguration(
                        databaseName: "pergo",
                        buildProject: "AlertSense.Pergo.Data",
                )
        ],
        websites: [
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Pergo",
                        siteName: "pergo.alertsense.com",
                        //publishTransform: "prod-elm-aws",
                        publishTransform: [
                                (EnvironmentType.Safe):"safe-aws",
                        ],
                ),
        ],
        branches: [
                new BranchConfig(
                        branchName: "rc-1.1.0",
                        version: "1.1.0",
                        //prefix: "dev"
                ),
        ],
        stacks: ConfigurationFactory.getStackConfig(),
        tests: [

        ]
)

ProjectPipelineBuilder.buildIntegrationJobs(this,config)

ProjectPipelineBuilder.buildDeliveryJobs(this,config)




