package alertsense

import com.alertsense.config.BranchConfig
import com.alertsense.config.Configuration
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.DatabaseConfiguration
import com.alertsense.config.EnvironmentType
import com.alertsense.config.ProjectConfiguration
import com.alertsense.builder.ProjectPipelineBuilder
import com.alertsense.config.StackConfig
import com.alertsense.config.TestProjectConfiguration
import com.alertsense.config.TestProjectGateType
import com.alertsense.config.WebsiteConfiguration

def config = new Configuration(
        name: "hickory",
        repoPath: ".",
        baseDirectory: ".\\src",
        solutionFile: "AlertSense.Hickory.sln",
        databaseName: "prod-hickory",
        websiteName: "hickory.alertsense.com",
        gitRepository: "git@bitbucket.org:AlertSense/Hickory.git",
        gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
        nugetConfig: ConfigurationFactory.getNugetConfig(),
        buildProjects: [
                "AlertSense.Hickory.AdminWebsite" : new ProjectConfiguration(
                        projectName: "AlertSense.Hickory.AdminWebsite",
                        projectFile: "AlertSense.Hickory.AdminWebsite.csproj",
                        projectFolder: ".\\src\\App\\AlertSense.Hickory.AdminWebsite",
                ),
                "AlertSense.Hickory.PublicWeb" : new ProjectConfiguration(
                        projectName: "AlertSense.Hickory.PublicWeb",
                        projectFile: "AlertSense.Hickory.PublicWeb.csproj",
                        projectFolder: ".\\src\\App\\AlertSense.Hickory.PublicWeb",
                ),
                "ImportClient" : new ProjectConfiguration(
                        projectName: "ImportClient",
                        projectFile: "ImportClient.csproj",
                        projectFolder: ".\\src\\App\\ImportClient",
                ),
                "AlertSense.Hickory.Data.Setup": new ProjectConfiguration(
                        projectFile: "AlertSense.Hickory.Data.Setup.csproj",
                        projectFolder: ".\\src\\Dal\\AlertSense.Hickory.Data.Setup",
                        publishNuget: true
                )
        ],
        databases: [
                new DatabaseConfiguration(
                        databaseName: "prod-hickory",
                        buildProject: "AlertSense.Hickory.Data.Setup",
                        profiles: ["static","development"] //,"DouglasR911Test"]
                )
        ],
        websites: [
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Hickory.AdminWebsite",
                        siteName: "hickory.alertsense.com",
                        publishTransform: [
                                (EnvironmentType.Production):"production-admin-aws",
                                (EnvironmentType.Dr):"integration-admin-dr",
                                //(EnvironmentType.DrIntegration):"integration-admin-dr",
                                (EnvironmentType.Integration):"integration-admin-aws",
                                (EnvironmentType.Safe):"safe-admin-aws",
                        ],
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                // internal service urls
                                parameters.put("/configuration/alertSense/aspen/apiClient/@baseUri", stack.hostUri("https","aspen",""))
                                parameters.put("/configuration/appSettings/add[@key='capUrl']/@value", stack.hostUri("https","cap","/"))

                                // rabbit mq settings
                                parameters.put("/configuration/RabbitMQ/RabbitMQGeneral/add[@key='RabbitMqServerUrl']/@value", "${-> stack.mqServerConfig.connectionString}")
                                parameters.put("/configuration/RabbitMQ/RabbitMQGeneral/add[@key='RabbitMqUseSsl']/@value", "${-> stack.mqServerConfig.useSsl}")
                                parameters.put("/configuration/RabbitMQ/RabbitMQProduction/add[@key='RabbitMQProdUser']/@value", "${-> stack.mqServerConfig.username}")
                                parameters.put("/configuration/RabbitMQ/RabbitMQProduction/add[@key='RabbitMQProdPassword']/@value", "${-> stack.mqServerConfig.password}")

                                // blob storage
                                parameters.put("/configuration/connectionStrings/add[@name='AzureStorageConnection']/@connectionString", "DefaultEndpointsProtocol=https;AccountName=${->stack.blobStorage.name};AccountKey=${->stack.blobStorage.key}")

                                // logging for email
                                parameters.put("/configuration/log4net/appender[@name='SmtpAppender']/to/@value", "${->stack.devOpsEmailAddress}")
                                parameters.put("/configuration/log4net/appender[@name='SmtpAppender']/from/@value", "admin+aws-${-> stack.name}@alertsense.com")

                                // only apply these deploy settings if we're not targeting production
                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        // url settings
                                        parameters.put("/configuration/appSettings/add[@key='UrlHttps']/@value", stack.hostUri("https","admin",""))
                                        parameters.put("/configuration/appSettings/add[@key='UrlAdminSystem']/@value", stack.hostUri("https","admin",""))
                                        parameters.put("/configuration/appSettings/add[@key='UrlExternalRequests']/@value", stack.hostUri("https","admin",""))
                                        parameters.put("/configuration/appSettings/add[@key='StorageDomain']/@value", stack.hostUri("https","storage","").replace("https://",""))
                                        parameters.put("/configuration/appSettings/add[@key='ElmDomainOrigin']/@value", stack.hostUri("https","elm",""))
                                        parameters.put("/configuration/appSettings/add[@key='ElmUrl']/@value" , stack.hostUri("https","elm",""))
                                        parameters.put("/configuration/appSettings/add[@key='TokenCookieDomain']/@value", ".alertsense.net")

                                        parameters.put("/configuration/alertSense/hickory/alertSignup/@signupFormUrl", stack.hostUri("https","public","/SignUp/public.aspx"))

                                        // social
                                        parameters.put("/configuration/facebookConnectionInfo/add[@key='appID']/@value","473455729499939")
                                        parameters.put("/configuration/facebookConnectionInfo/add[@key='appSecret']/@value","81d0d089a91e04d20c2b4fef30514c42")
                                }
                                return parameters
                        },
                        //recycleBeforeDeploy: true,
                ),
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Hickory.PublicWeb",
                        siteName: "public.alertsense.com",
                        publishTransform: [
                                (EnvironmentType.Production):"production-public-aws",
                                (EnvironmentType.Dr):"production-public-dr",
                                (EnvironmentType.DrIntegration):"integration-public-dr",
                                (EnvironmentType.Integration):"integration-public-aws",
                                (EnvironmentType.Safe):"safe-public-aws",
                        ],
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                parameters.put("/configuration/appSettings/add[@key='ApiServiceUrl']/@value", stack.hostUri("https", "admin", "/api/"))

                                // only apply these deploy settings if we're not targeting production
                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        parameters.put("/configuration/appSettings/add[@key='UrlHttps']/@value", "https://public-${-> stack.name}.aws.alertsense.net:8443")
                                        parameters.put("/configuration/appSettings/add[@key='UrlAdminSystem']/@value", "https://public-${-> stack.name}.aws.alertsense.net:8443")
                                        parameters.put("/configuration/appSettings/add[@key='UrlExternalRequests']/@value", "https://public-${-> stack.name}.aws.alertsense.net:8443")
                                }
                                return parameters
                        }
                ),
        ],
        branches: [
                new BranchConfig(
                        branchName: "hotfix-2.18.2",
                        version: "2.18.2",
                        //prefix: "dev"
                ),
        ],
        stacks: ConfigurationFactory.getStackConfig(),
        tests: [
                new TestProjectConfiguration(
                        nunitTestFile: ".\\src\\Tests\\hickory_unit_tests.nunit",
                        resultsLabel: "postBuild",
                        testType: TestProjectGateType.Build,
                        excludedCategories: ["Integration","Database"]
                )
        ]
)

ProjectPipelineBuilder.buildIntegrationJobs(this,config)

ProjectPipelineBuilder.buildDeliveryJobs(this,config)




