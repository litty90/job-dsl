package alertsense

import com.alertsense.builder.CiJobBuilder
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.GitConfig
import com.alertsense.steps.BatchStep
import com.alertsense.steps.NugetInstallStep
import utilities.AlertSense

/**
 * Created by joey on 11/9/2015.
 */

String basePath = 'operational-jobs'

folder(basePath) {
    description ''
}

basePath += "/sftp"

folder(basePath) {
        description 'Sftp customer import jobs.'
}

new CiJobBuilder(
        name: "$basePath/import-members",
        scm: new GitConfig(
                gitBranch: "DEV-526",
                gitRepository: "git@bitbucket.org:AlertSense/gum.git",
                gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910"
        ),
        cronTrigger: "0 5 * * *",
        buildSteps: [
                new NugetInstallStep(
                        name: "install-package",
                        nugetConfig: ConfigurationFactory.getNugetConfig(),
                        packageName: "ImportClient",
                ),
                new BatchStep(command: "SftpProcessing\\SftpProcessing.bat")
        ],
        //cleanBeforeBuild: true,
        archiveArtifacts: true,
        //downstreamName: postBuildDownStreamJob
).build(this)