package alertsense

import com.alertsense.config.BranchConfig
import com.alertsense.config.Configuration
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.DatabaseConfiguration
import com.alertsense.config.DatabaseMigrator
import com.alertsense.config.EnvironmentType
import com.alertsense.config.NugetSource
import com.alertsense.config.ProjectConfiguration
import com.alertsense.builder.ProjectPipelineBuilder
import com.alertsense.config.StackConfig
import com.alertsense.config.WebsiteConfiguration

def config = new Configuration(
        name: "bamboo",
        repoPath: ".",
        baseDirectory: ".\\src",
        solutionFile: "AlertSense CAP.sln",
        websiteName: "bamboo.alertsense.com",
        gitRepository: "git@bitbucket.org:AlertSense/Bamboo.git",
        gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
        nugetConfig: ConfigurationFactory.getNugetConfig(),
        buildProjects: [
                "AlertSense.Website" : new ProjectConfiguration(
                        projectName: "AlertSense.Website",
                        projectFile: "AlertSense.Website.csproj",
                        projectFolder: ".\\src\\Applications\\AlertSense.Website",
                ),
                "AlertSense.Web.TtsService" : new ProjectConfiguration(
                        projectName: "AlertSense.Web.TtsService",
                        projectFile: "AlertSense.Web.TtsService.csproj",
                        projectFolder: ".\\src\\Applications\\AlertSense.Web.TtsService",
                ),
                "AlertSense.Core": new ProjectConfiguration(
                        projectFile: "AlertSense.Core.csproj",
                        projectFolder: ".\\src\\libraries\\AlertSense.Core",
                        publishNuget: true
                )
        ],
        databases: [
                new DatabaseConfiguration(
                        databaseName: "AlertSenseCap_Generated",
                        buildProject: "AlertSense.Core",
                        migrationType: DatabaseMigrator.EntityFrameworkMigration
                )
        ],
        websites: [
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Website",
                        siteName: "bamboo.alertsense.com",
                        //publishTransform: "prod-cap-aws",
                        publishTransform: [
                                (EnvironmentType.Production):"production-cap-aws",
                                (EnvironmentType.Dr):"integration-cap-dr",
                                (EnvironmentType.Integration):"integration-cap-aws",
                                (EnvironmentType.Safe):"safe-cap-aws",
                        ],
                        appOffline: true,
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                // error emails
                                parameters.put("/configuration/elmah/errorMail/@from", "bamboo+aws-${ -> stack.name}@alertsense.com")
                                parameters.put("/configuration/elmah/errorMail/@to", "${->stack.devOpsEmailAddress}")
                                parameters.put("/configuration/elmah/errorMail/@subject", "AWS-${ -> stack.name} - [ERROR] {1} - {0}")
                                parameters.put("/configuration/appSettings/add[@key='EasHistoryUrl']/@value",stack.hostUri("https","admin",""))

                                parameters.put("/configuration/connectionStrings/add[@name='AzureStorageConnection']/@connectionString", "DefaultEndpointsProtocol=https;AccountName=${->stack.blobStorage.ttsName};AccountKey=${->stack.blobStorage.ttsKey}")

                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        parameters.put("/configuration/appSettings/add[@key='TtsServiceUrl']/@value", "http://localhost:56816")
                                        parameters.put("/configuration/appSettings/add[@key='CertConverterService']/@value", "http://localhost")
                                        parameters.put("/configuration/appSettings/add[@key='StorageDomain']/@value", "storage-tts-${stack.name}.aws.alertsense.net:8443")
                                }
                                return parameters
                        }
                ),
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Web.TtsService",
                        siteName: "tts.alertsense.com",
                        publishTransform: [
                                (EnvironmentType.Production):"debug",
                                (EnvironmentType.Dr):"debug",
                                (EnvironmentType.Integration):"debug",
                                (EnvironmentType.Safe):"debug",
                        ],
                        appOffline: true,
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                parameters.put("/configuration/connectionStrings/add[@AlertSenseCap]/@connectionString", "Data Source=db,1433;Initial Catalog=AlertSenseCap_generated;Integrated Security=True")
                                parameters.put("/configuration/connectionStrings/add[@name='AzureStorageConnection']/@connectionString", "DefaultEndpointsProtocol=https;AccountName=${->stack.blobStorage.ttsName};AccountKey=${->stack.blobStorage.ttsKey}")


                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        parameters.put("/configuration/appSettings/add[@key='StorageDomain']/@value", "storage-tts-${stack.name}.aws.alertsense.net:8443")
                                }
                                return parameters
                        }
                ),
        ],
        branches: [
                new BranchConfig(
                        branchName: "dev",
                        version: "1.9.0",
                        prefix: "dev"
                ),
                new BranchConfig(
                        branchName: "hotfix-1.10.1",
                        version: "1.10.1",
                ),
        ],
        stacks: ConfigurationFactory.getStackConfig(),
        tests: [

        ]
)

ProjectPipelineBuilder.buildIntegrationJobs(this,config)

ProjectPipelineBuilder.buildDeliveryJobs(this,config)




