
package alertsense

import com.alertsense.config.BranchConfig
import com.alertsense.config.Configuration
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.DatabaseConfiguration
import com.alertsense.config.EnvironmentType
import com.alertsense.config.ProjectConfiguration
import com.alertsense.builder.ProjectPipelineBuilder
import com.alertsense.config.StackConfig
import com.alertsense.config.WebsiteConfiguration


def config = new Configuration(
        name: "elm",
        repoPath: ".",
        baseDirectory: ".\\src",
        solutionFile: "AlertSense.Elm.sln",
        databaseName: "prod-elm",
        websiteName: "elm.alertsense.com",
        gitRepository: "git@bitbucket.org:AlertSense/Elm.git",
        gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
        nugetConfig: ConfigurationFactory.getNugetConfig(),
        buildProjects: [
                "AlertSense.Elm.MappingWebsite" : new ProjectConfiguration(
                        projectName: "AlertSense.Elm.MappingWebsite",
                        projectFile: "AlertSense.Elm.MappingWebsite.csproj",
                        projectFolder: ".\\src\\App\\AlertSense.Elm.MappingWebsite",
                ),
                "AlertSense.Elm.Data.Setup": new ProjectConfiguration(
                        projectFile: "AlertSense.Elm.Data.Setup.csproj",
                        projectFolder: ".\\src\\Dal\\AlertSense.Elm.Data.Setup",
                        publishNuget: true
                )
        ],
        databases: [
                new DatabaseConfiguration(
                        databaseName: "prod-elm",
                        buildProject: "AlertSense.Elm.Data.Setup",
                        profiles: ["ProductionElm"]
                )
        ],
        websites: [
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Elm.MappingWebsite",
                        siteName: "elm.alertsense.com",
                        //publishTransform: "prod-elm-aws",
                        publishTransform: [
                                (EnvironmentType.Production):"production-elm-aws",
                                (EnvironmentType.Dr):"integration-elm-dr",
                                (EnvironmentType.Integration):"integration-elm-aws",
                                (EnvironmentType.Safe):"safe-elm-aws",
                        ],
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                parameters.put("/configuration/appSettings/add[@key='HickoryServiceUrl']/@value", "https://admin-${stack.name}.aws.alertsense.net:8443/api")

                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        // no production specific values yet
                                }
                                return parameters
                        }
                ),
        ],
        branches: [
                new BranchConfig(
                        branchName: "rc-1.4.0",
                        version: "1.4.0",
                        //prefix: "dev"
                ),
        ],
        stacks: ConfigurationFactory.getStackConfig(),
        tests: [

        ]
)

ProjectPipelineBuilder.buildIntegrationJobs(this,config)

ProjectPipelineBuilder.buildDeliveryJobs(this,config)




