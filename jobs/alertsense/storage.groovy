package alertsense

import com.alertsense.builder.CiJobBuilder
import com.alertsense.config.ConfigurationFactory
import com.alertsense.steps.BuildStep
import com.alertsense.steps.MsDeployConfigOnlyStep


def stacks = ConfigurationFactory.getStackConfig()

def name = "storage"

stacks.each { stack ->

        def basePath = "deploy"

        folder(basePath) {
                description: "folder for deploy jobs."
        }

        basePath += "/${stack.name}"

        folder(basePath) {
                description:
                "Deploy jobs for the stack ${stack.name}."
        }

        basePath += "/${name}"

        folder(basePath) {
                description:
                "Deploy jobs for ${name} within the stack ${stack.name}."
        }

        stack.webServerGroups.each { webServerGroup ->

                def deployWebName = "${basePath}/deploy-${webServerGroup.name}"

                List<BuildStep> storageSteps = []
                List<BuildStep> ttsStorageSteps = []

                webServerGroup.webServers.each { webServerName ->
                        storageSteps.add(new MsDeployConfigOnlyStep(
                                name: "storage-deploy",
                                deployWebsiteName: "storage.alertsense.com",
                                deployServerName: webServerName,
                                setParameters: ["/configuration/system.webServer/httpRedirect/@destination": "https://${-> stack.blobStorage.name}.blob.core.windows.net/"]
                        ))

                        ttsStorageSteps.add(new MsDeployConfigOnlyStep(
                                name: "tts-storage-deploy",
                                deployWebsiteName: "tts-storage.alertsense.com",
                                deployServerName: webServerName,
                                setParameters: ["/configuration/system.webServer/httpRedirect/@destination": "https://${-> stack.blobStorage.ttsName}.blob.core.windows.net/"]
                        ))
                }

                new CiJobBuilder(
                        name: "${deployWebName}_storage",
                        buildSteps: storageSteps,
                ).build(this)


                new CiJobBuilder(
                        name: "${deployWebName}_tts-storage",
                        buildSteps: ttsStorageSteps,
                ).build(this)

        }
}