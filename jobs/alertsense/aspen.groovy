package alertsense

import com.alertsense.config.BranchConfig
import com.alertsense.config.Configuration
import com.alertsense.config.ConfigurationFactory
import com.alertsense.config.DatabaseConfiguration
import com.alertsense.config.EnvironmentType
import com.alertsense.config.ProjectConfiguration
import com.alertsense.builder.ProjectPipelineBuilder
import com.alertsense.config.StackConfig
import com.alertsense.config.WebsiteConfiguration

def config = new Configuration(
        name: "aspen",
        repoPath: ".",
        baseDirectory: ".",
        solutionFile: "AlertSense.Aspen.sln",
        databaseName: "aspen",
        websiteName: "aspen.alertsense.com",
        gitRepository: "git@bitbucket.org:AlertSense/aspen.git",
        gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
        nugetConfig: ConfigurationFactory.getNugetConfig(),
        nugetSolutionPath: ".\\.nuget\\nuget.exe",
        nugetSolutionConfigPath: ".\\.nuget\\Nuget.config",
        buildProjects: [
                "AlertSense.Aspen" : new ProjectConfiguration(
                        projectName: "AlertSense.Aspen",
                        projectFile: "AlertSense.Aspen.csproj",
                        projectFolder: ".\\AlertSense.Aspen",
                ),
                "AlertSense.Aspen.Workflow" : new ProjectConfiguration(
                        projectName: "AlertSense.Aspen.Workflow",
                        projectFile: "AlertSense.Aspen.Workflow.csproj",
                        projectFolder: ".\\AlertSense.Aspen.Workflow",
                ),
                "AlertSense.Aspen.Data": new ProjectConfiguration(
                        projectFile: "AlertSense.Aspen.Data.csproj",
                        projectFolder: ".\\AlertSense.Aspen.Data",
                        publishNuget: true
                )
        ],
        databases: [
                new DatabaseConfiguration(
                        databaseName: "aspen",
                        buildProject: "AlertSense.Aspen.Data",
                        profiles: []
                )
        ],
        websites: [
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Aspen",
                        siteName: "aspen.alertsense.com",
                        //publishTransform: "production-aws",
                        publishTransform: [
                                (EnvironmentType.Production):"production-aws",
                                (EnvironmentType.Dr):"integration-dr",
                                (EnvironmentType.Integration):"integration-aws",
                                (EnvironmentType.Safe):"safe-aws",
                        ],
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                parameters.put("/configuration/alertSense/aspen/elasticacheMemcachedClusterConfiguration/@elasticacheConfigurationHostname", "${-> stack.elastiCacheHost}")
                                // rabbit mq
                                parameters.put("/configuration/alertSense/aspen/messageQueue/@connectionString", "${-> stack.mqServerConfig.connectionString}")
                                parameters.put("/configuration/alertSense/aspen/messageQueue/@publisherConfirms", "${-> stack.mqServerConfig.publisherConfirms}")
                                parameters.put("/configuration/alertSense/aspen/messageQueue/@useSsl", "${-> stack.mqServerConfig.useSsl}")
                                parameters.put("/configuration/alertSense/aspen/messageQueue/@username", "${-> stack.mqServerConfig.username}")
                                parameters.put("/configuration/alertSense/aspen/messageQueue/@password", "${-> stack.mqServerConfig.password}")
                                // logging
                                parameters.put("/configuration/log4net/appender[@name='SmtpAppender']/to/@value", "${-> stack.devOpsEmailAddress}")
                                parameters.put("/configuration/log4net/appender[@name='SmtpAppender']/from/@value", "aspen+aws-${-> stack.name}@alertsense.com")

                                // stack urls
                                parameters.put("/configuration/alertSense/aspen/voice/voiceProviders/add[@name='plivo']/@answerDomain", stack.hostUri("https","aspen",""))
                                parameters.put("/configuration/alertSense/aspen/voice/voiceProviders/add[@name='callfire']/@answerDomain", stack.hostUri("https","aspen",""))
                                parameters.put("/configuration/alertSense/aspen/voice/voiceProviders/add[@name='voxeo']/@answerDomain", stack.hostUri("https","aspen",""))
                                parameters.put("/configuration/alertSense/aspen/text/textProviders/add[@name='twilio']/@answerDomain", stack.hostUri("https","aspen",""))

                                // internal service urls
                                parameters.put("/configuration/alertSense/aspen/workflow/@serviceUri", "http://aspen-workflow-${-> stack.name}.aws.alertsense.net")

                                if(stack.environmentType != (EnvironmentType.Production)) {
                                        // short codes
                                        parameters.put("/configuration/alertSense/aspen/text/shortCodes/add[@country='US']/@code", "${-> stack.smsShortCodes['US']}")
                                        parameters.put("/configuration/alertSense/aspen/text/shortCodes/add[@country='CA']/@code", "${-> stack.smsShortCodes['CA']}")
                                }


                            return parameters
                        }
                ),
                new WebsiteConfiguration(
                        buildProject: "AlertSense.Aspen.Workflow",
                        siteName: "aspen-workflow.alertsense.com",
                        //publishTransform: "production-aws",
                        publishTransform: [
                                (EnvironmentType.Production):"production-aws",
                                (EnvironmentType.Dr):"integration-dr",
                                (EnvironmentType.Integration):"integration-aws",
                                (EnvironmentType.Safe):"safe-aws",
                        ],
                        // This closure is needed so that the specific stack config is used for each website we make for each of the stacks
                        parametersClosure: { StackConfig stack ->
                                Map parameters = [:]

                                parameters.put("/configuration/alertSense/aspen/apiClient/@baseUri", stack.hostUri("https","aspen",""))

                                if(stack.environmentType != (EnvironmentType.Production)) {
                                }


                                return parameters
                        }
                ),
        ],
        branches: [
                new BranchConfig(
                        branchName: "hotfix-1.11.3",
                        version: "1.11.3",
                        //prefix: "dev"
                ),
        ],
        stacks: ConfigurationFactory.getStackConfig(),
        tests: [

        ]
)

ProjectPipelineBuilder.buildIntegrationJobs(this,config)

ProjectPipelineBuilder.buildDeliveryJobs(this,config)
