import com.alertsense.MsDeployCiJobBuilder

//
// Project Specific Variables
//
def basePath = 'hickory'
def workspace = 'Y:\\Jenkins\\workspace\\hickory'
def solutionDirectory = "$workspace\\src"

// web
def publishProfile = 'prod-public'
def siteName = 'public.alertsense.com'
def projectDirectory = "${solutionDirectory}\\App\\AlertSense.Hickory.PublicWeb"
def projectFile = "AlertSense.Hickory.PublicWeb.csproj"

def parameters = ["stack" : stack]

//
// Basically Constants
//
String parametersTemplateFile = "${projectDirectory}\\parameters-aws.xml"
String parametersFile = "${projectDirectory}\\parameters.xml"

//
// Start of DSL
//
folder(stack) {
        description "CI jobs for ${stack}"
}

folder("$stack/$basePath") {
        description "CI jobs for ${stack}/${basePath}"
}

def jobName ="$stack/$basePath/deploy-public"
def nextJob = null
new MsDeployCiJobBuilder(
        name: jobName,
        description: "Deploys and package ${basePath}.",
        workspace: workspace,
        projectDirectory: projectDirectory,
        projectFile: projectFile,
        publishProfile: publishProfile,
        parametersTemplateFile: parametersTemplateFile,
        parametersFile: parametersFile,
        paramtersBinding: parameters,
        siteName: siteName,
        deployUrl: webServer,
        downStreamPublisher:  nextJob
).build(this)