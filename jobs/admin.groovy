
job('create-pipes') {

    description '''
1. Creates the gerrit projects
2. Creates the job dsl file.
3. Manual Step:  Please run the job named 'job-dsl' to create the jenkins jobs.

note:  if the jenkins jobs need to be customized, checkout git@bitbucket.org:AlertSense/job-dsl.git and edit the job.
'''

    label 'master'

    parameters {
        stringParam('PROJECT_NAME', '', 'The name of the project, usually the git repo name.  Please no spaces/funky characters.  examples: elm, hickory')
        stringParam('GIT_REPO_URL', '', 'The bitbucket repository URL that is used for cloning the repo')
        stringParam('GERRIT_REPO_URL', 'ssh://jenkins@gerrit.dev.aws.alertsense.net:29418', 'Use the default, unless you know what you are doing... ')
        stringParam('BRANCHES', 'dev,staging,production', 'The branch names that the pipes will use.  ')
        stringParam('GIT_REF_BASE','','The starting git reference in the GIT_REPO_URL.  This can be either the BRANCH or a different reference.  The remote is called origin.  To have master be the base, origin/master would be the git reference.  Note, if the branch exists in either gerrit or bitbucket, it will be rewritten to point to this reference.')
        stringParam('REVIEW_CMD', '.\\ci\\review.bat', 'The review command.')
        stringParam('INTEGRATE_CMD', '.\\ci\\integrate.bat', 'The integrate command.')
        stringParam('DEPLOY_CMD', '.\\ci\\deploy.bat', 'The deploy command.')
    }

    scm {
        git {
            remote {
                name 'origin'
                url 'git@bitbucket.org:AlertSense/cibuild.git'
                credentials("7965899b-376f-4645-9639-a893315840a5")
            }
            clean()
            branch 'master'
        }
    }

    steps {
        environmentVariables {
            propertiesFile('/var/lib/jenkins/env.sh')
        }
        shell('./create-pipe.sh')
    }

}

job('job-dsl') {
    description 'Creates all the jobs'
    label 'master'

    scm {
        git {
            remote {
                name 'origin'
                url 'git@bitbucket.org:AlertSense/job-dsl.git'
                credentials("7965899b-376f-4645-9639-a893315840a5")
            }
            branch 'test'
        }
    }

    steps {
        dsl {
            external 'jobs/**.groovy'
            removeAction 'DELETE'
            removeViewAction 'DELETE'
        }

    }

}

job('code-review-backup') {

    description '''
1. Takes the backup of jenkins and gerrit configurations
2. Uploads to S3 bucket
'''

    label 'master'

    scm {
        git {
            remote {
                name 'origin'
                url 'git@bitbucket.org:AlertSense/cibuild.git'
                credentials("7965899b-376f-4645-9639-a893315840a5")
            }
            clean()
            branch 'master'
        }
    }

    triggers {
      cron("H H * * *")
    }

    steps {
        shell('./backup/create_backup.sh')
    }

}
listView('admin') {

    description('Administration jobs')
    jobs {
        name 'job-dsl'
        name 'create-pipes'
        name 'code-review-backup'
    }

    columns {
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
        buildButton()
    }
}
