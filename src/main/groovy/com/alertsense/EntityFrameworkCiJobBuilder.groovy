package com.alertsense

import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 *  Example Class for creating a Grails create
 */
class EntityFrameworkCiJobBuilder {

    String name
    String description
    String workspace
    String solutionDirectory
    String assemblyDirectory
    String assembly
    String connection
    String downStreamPublisher;

    // Constants
    private def args = '/connectionProviderName="System.Data.SqlClient"'

    // msbuild


    Job build(DslFactory dslFactory) {
        dslFactory.job(name) {
            it.description description

            if(workspace)
            {
                customWorkspace(workspace)
            }
            logRotator(60,-1,-1,-1)

            steps {
                batchFile(""" "${solutionDirectory}\\packages\\EntityFramework.4.3.1\\tools\\Migrate.exe" "${assembly}" ${args}  /StartupDirectory="${assemblyDirectory}" /connectionString="${connection}" """.stripIndent().trim())
            }

            if(downStreamPublisher) {
                publishers {
                    downstream(downStreamPublisher, 'SUCCESS')
                }
            }

        }
    }
}
