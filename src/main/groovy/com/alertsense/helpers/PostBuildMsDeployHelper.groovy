package com.alertsense.helpers

/**
 * Created by joey on 10/29/2015.
 */
class PostBuildMsDeployHelper {
    static void ParseResults(manager) {
        if(manager.logContains(".*No matches were found.*")) {
            manager.addWarningBadge("Web.config transform Failure")
            manager.createSummary("warning.gif").appendText("<h2>Web.config transform Failure</h2><p>A parameter value for the web.config transformation has failed. Please check the log output for more information.</p>", false, false, false, "red")
            manager.buildUnstable()
        }
    }
}
