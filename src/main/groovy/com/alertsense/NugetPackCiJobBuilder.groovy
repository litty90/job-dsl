package com.alertsense

import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 *  Example Class for creating a Grails create
 */
class NugetPackCiJobBuilder {

    String name
    String description
    String workspace
    String ownerAndProject
    String credential = '19f4b472-3a57-46b6-ad4b-0f6441867910'
    String gitBranch = 'master'
    String pollScmSchedule = '*/5 * * * *'
    String solutionDirectory
    String solutionFile
    String downStreamPublisher
    List<String> emails


    // Constants
    private def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
    private def msBuildArgs = '/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug'

    Job build(DslFactory dslFactory) {
        dslFactory.job(name) {
            it.description description

            if(workspace)
            {
                customWorkspace(workspace)
            }
            logRotator(60,-1,-1,-1)

            scm {
                git {
                    remote {
                        url(ownerAndProject)
                        credentials(credential)
                    }
                    branch(gitBranch)
                }
            }

            steps {
                batchFile(""" "${solutionDirectory}\\.nuget\\nuget.exe" restore "${solutionDirectory}\\${solutionFile}" """.stripIndent().trim())
                batchFile(""" ${msBuildExe} /t:Clean,Rebuild "${solutionDirectory}\\${solutionFile}" ${msBuildArgs} """.stripIndent().trim())
            }

            if(downStreamPublisher) {
                publishers {
                    downstream(downStreamPublisher, 'SUCCESS')
                }
            }
        }
    }
}
