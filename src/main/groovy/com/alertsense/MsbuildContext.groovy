package com.alertsense

import javaposse.jobdsl.dsl.Context

class MsbuildContext implements Context {


    String msBuildName
    String msBuildFile
    String cmdLineArgs
    boolean buildVariablesAsProperties = true
    boolean continueOnBuildFailure = false
    Closure configureBlock

    void tasks(String msBuildName) {
        this.msBuildName << msBuildName
    }


    /**
     * Allows direct manipulation of the generated XML. The {@code hudson.plugins.gradle.Gradle} node is passed into the
     * configure block.
     *
     * @see <a href="https://github.com/jenkinsci/job-dsl-plugin/wiki/The-Configure-Block">The Configure Block</a>
     */
    void configure(Closure closure) {
        this.configureBlock = closure
    }
}