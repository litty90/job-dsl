package com.alertsense.builder

import com.alertsense.StringParam
import com.alertsense.config.Configuration
import com.alertsense.config.GitConfig
import com.alertsense.steps.BuildStep
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job
import utilities.AlertSense


class CiJobBuilder {

    String name
    Configuration config
    GitConfig scm
    List<StringParam> parameters
    List<BuildStep> buildSteps
	String downstreamName
    String buildToken
    String cronTrigger
    boolean cleanBeforeBuild = false
	boolean archiveArtifacts = false
    String validateTestResultsPattern

    Job build(DslFactory dslFactory) {

        dslFactory.job(name) {

            label('windows-jenkins-slave')

            logRotator {
                numToKeep(50)
                artifactsNumToKeep(10)
            }
            it.description "Build job for ${name}"
            if(cleanBeforeBuild) {
                wrappers {
                    preBuildCleanup()
                }
            }
            if (parameters) {
                parameters {
                    parameters.each { param ->
                        stringParam(param.name, param.defaultValue, param.description)
                    }
                }
            }
            if(scm) {
                scm {
                    git {
                        remote {
                            url(scm.gitRepository)
                            credentials(scm.gitCredentialId)
                        }
                        branch(scm.gitBranch)
						if(scm.clean) {
							clean(true)
						}
                        clean(true)
                        shallowClone(true)
                        remotePoll(true)
                    }
                }
            }
            if(cronTrigger) {
                triggers {
                    cron cronTrigger
                }
            }
            //if(buildToken) {
            //    authenticationToken(buildToken)
            if(buildSteps) {
                steps {
                    buildSteps.each { step ->
                        step.create delegate
                    }
                }
            }
            publishers {
                if(archiveArtifacts) {
                    archiveArtifacts {
                        pattern('**/*.*')
                        onlyIfSuccessful(true)
                    }
                }
                if(validateTestResultsPattern) {
                    configure AlertSense.NunitTest(validateTestResultsPattern)
                }
                if(downstreamName) {
                    downstream(downstreamName,'SUCCESS')
                }
                if(scm && scm.gitTagBuild) {
                    git {
                        pushOnlyIfSuccess()
                        forcePush()
                        tag("origin","v${scm.gitTagVersion}") {
                            message("Built by Alfred version ${scm.gitTagVersion} from \$BUILD_TAG")
                            create()
                            update()
                        }

                    }
                }
            }

        }
    }
}
