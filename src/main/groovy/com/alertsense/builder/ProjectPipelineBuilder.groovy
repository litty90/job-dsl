package com.alertsense.builder

import com.alertsense.StringParam
import com.alertsense.config.Configuration
import com.alertsense.config.DatabaseMigrator
import com.alertsense.config.GitConfig
import com.alertsense.steps.BuildStep
import com.alertsense.steps.CopyArtifactStep
import com.alertsense.steps.EntityFrameworkMigratorStep
import com.alertsense.steps.FluentMigratorStep
import com.alertsense.steps.MsDeployStep
import com.alertsense.steps.NugetInstallStep
import com.alertsense.steps.NugetPackStep
import com.alertsense.steps.NugetPushStep
import com.alertsense.steps.NunitTestStep
import com.alertsense.steps.RestoreNugetBuild
import com.alertsense.steps.SolutionBuild
import com.alertsense.steps.ResetBuildNumberStep

/**
 * Created by joey on 10/16/2015.
 */
class ProjectPipelineBuilder {

    static void buildIntegrationJobs(dslFactory, Configuration config) {

        dslFactory.folder("build") {
            description: "Contains all the build jobs."
        }

        dslFactory.folder("build/${config.name}") {
            description: "Contains all the jobs for the ${config.name} project."
        }

        config.branches.each { branch ->

            def branchFolderName = "build/${config.name}/${branch.branchName}" //-${branch.version}"

            dslFactory.folder(branchFolderName) {
                description: "Build jobs for the $branch branch of the ${config.name} project."
            }

            def buildName = "${branchFolderName}/build-artifacts" //-${branch.version}"
            def publishName = "${branchFolderName}/publish-artifacts" //-${branch.version}"
            def postBuildTestName = "${branchFolderName}/test-artifacts" //-${branch.version}"
            def resetBuildNumberName = "${branchFolderName}/reset-build-number" //-${branch.version}"

            def postBuildTests = config.getPostBuildTests()

            def packageVersion = branch.getPackageVersion()

            def gitConfig = new GitConfig(
                    gitRepository: config.gitRepository,
                    gitCredentialId: config.gitCredentialId,
                    gitBranch: branch.branchName,
                    gitTagBuild: true,
                    gitTagVersion: packageVersion.replace('%BUILD_NUMBER%','$BUILD_NUMBER')
            )

            // default steps to always be done with every project, restore nuget packages for the solution and build with msbuild
            def buildArtifactSteps = [
                    new RestoreNugetBuild(
                            name: "restore-packages",
                            nugetPath: config.nugetSolutionPath,
                            nugetConfigPath: config.nugetSolutionConfigPath,
                            targetFolder: config.baseDirectory,
                            targetFile: config.solutionFile,
                    ),
                    new SolutionBuild(
                            name: "solution-create",
                            rebuild: true,
                            clean: true,
                            targetFolder: config.baseDirectory,
                            targetFile: config.solutionFile,
                            version: packageVersion,
                            msBuildArgs: ["/p:VisualStudioVersion=12.0", "/tv:4.0", "/m:1", "/nologo", "/p:Configuration=Debug", "/v:q", "/p:RunOctoPack=true","/p:OctoPackEnforceAddingFiles=true"]
                    ),

            ]

            // loop through all of our projects that need to be packed as nuget packages and pack them
            config.buildProjects.each { key, project ->
                if(project.publishNuget) {
                    buildArtifactSteps.add(new NugetPackStep(
                            name: key,
                            nugetConfig: config.nugetConfig,
                            targetFolder: project.projectFolder,
                            targetFile: project.projectFile,
                            version: packageVersion,

                    ))
                }
            }

            def postBuildDownStreamJob = publishName

            // handle post build test steps to be run
            if(postBuildTests) {
                // set the name of the downstream job to be called after building
                postBuildDownStreamJob = postBuildTestName

                def buildTestSteps = [
                    new CopyArtifactStep(
                            buildname: buildName,
                            // only copy nuget packages
                            targetFolder: "",
                            patterns: "**/src/Tests/**/bin/**/*.*,**/*.nunit",
                            flattenDirectories: false
                    )
                ]

                postBuildTests.each { testConfig ->
                    buildTestSteps.add(new NunitTestStep(testConfig: testConfig))
                }

                new CiJobBuilder(
                        name: postBuildTestName,
                        cleanBeforeBuild: true,
                        validateTestResultsPattern: "**/Nunit.*.xml",
                        buildSteps: buildTestSteps,
                        downstreamName: publishName
                ).build(dslFactory)
            }

            // create our artifact build job for the branch
            new CiJobBuilder(
                    name: buildName,
                    scm: gitConfig,
                    buildSteps: buildArtifactSteps,
                    //cleanBeforeBuild: true,
                    archiveArtifacts: true,
                    downstreamName: postBuildDownStreamJob,
            ).build(dslFactory)


            // create the publish artifact job for the branch
            new CiJobBuilder(
                    name: publishName,
                    cleanBeforeBuild: true,
                    buildSteps: [
                            new CopyArtifactStep(
                                    buildname: buildName,
                                    // only copy nuget packages
                                    patterns: "**/obj/**/*.nupkg",
                                    targetFolder: "packages",
                                    flattenDirectories: true
                            ),
                            new NugetPushStep(
                                    name: "packages",
                                    nugetConfig: config.nugetConfig,
                                    targetFolder: ".",
                                    packageFolder: "packages",
                            ),
                    ]
            ).build(dslFactory)

            new CiJobBuilder(
                    name: resetBuildNumberName,
                    parameters: [new StringParam(
                            name: "BUILDNUMBER",
                            defaultValue: "1",
                            description: "Reset the build number for $buildName."
                    )],
                    buildSteps: [
                            new ResetBuildNumberStep(buildName: buildName)
                    ]
            ).build(dslFactory)
        }
    }

    static void buildDeliveryJobs(dslFactory, Configuration config) {

        config.stacks.each { stack ->

            def stackFolderName = "deploy"

            dslFactory.folder(stackFolderName) {
                description: "folder for deploy jobs."
            }

            stackFolderName += "/${stack.name}"

            dslFactory.folder(stackFolderName) {
                description: "Deploy jobs for the stack ${stack.name}."
            }

            stackFolderName += "/${config.name}"

            dslFactory.folder(stackFolderName) {
                description: "Deploy jobs for ${config.name} within the stack ${stack.name}."
            }

            def buildToken = "${config.name}-${stack.name}"

            // build a database migration job for each database registered in the configuration
            config.databases.each { database ->

                // get the database from our build projects list
                //ProjectConfiguration databaseProject = config.buildProjects[database.buildProject]

                def migrateName = "${stackFolderName}/migrate-${database.databaseName}"

                BuildStep migratorStep

                // determine which database migration tool to use
                if(database.migrationType == DatabaseMigrator.FluentMigrator) {
                    migratorStep = new FluentMigratorStep(
                            name: "database-publish",
                            targetFile: "${database.buildProject}.dll",
                            targetFolder: "${database.buildProject}.%VERSION%\\lib\\net45",
                            packageFolder: ".",
                            connectionString: "Data Source=${stack.databaseServer};Database=${database.databaseName};Integrated Security=True",
                            profiles: database.profiles
                    )
                }
                else if(database.migrationType == DatabaseMigrator.EntityFrameworkMigration) {
                    migratorStep = new EntityFrameworkMigratorStep(
                            name: "database-publish",
                            targetFile: "${database.buildProject}.dll",
                            targetFolder: "${database.buildProject}.%VERSION%\\lib\\net45",
                            packageFolder: ".",
                            connectionString: "Data Source=${stack.databaseServer};Database=${database.databaseName};Integrated Security=True",
                    )
                }


                new CiJobBuilder(
                        name: migrateName,
                        cleanBeforeBuild: true,
                        parameters: [new StringParam(
                                name: "VERSION",
                                defaultValue: "",
                                description: "The version of the \"${database.buildProject}\" data migration package to publish."
                        )],
                        buildSteps: [
                                new NugetInstallStep(
                                        name: "install-package",
                                        nugetConfig: config.nugetConfig,
                                        packageName: database.buildProject,
                                        version: "%VERSION%"
                                ),
                                migratorStep
                        ]
                ).build(dslFactory)
            }

            stack.webServerGroups.each { webServerGroup ->
                config.websites.each { website ->


                    def deployWebName = "${stackFolderName}/deploy-${webServerGroup.name}_${website.siteName}"

                    def setParameters = [:]

                    if(website.parametersClosure && !stack.ignoreSetParameters) {
                        setParameters = website.parametersClosure(stack)
                    }

                    List<BuildStep> webBuildSteps = [
                            new NugetInstallStep(
                                    name: "install-package",
                                    nugetConfig: config.nugetConfig,
                                    packageName: website.buildProject,
                                    //packageFolder: "${config.repoPath}\\deploy-packages",
                                    version: "%VERSION%"
                            )]

                    def publishTransform = website.publishTransform[(stack.environmentType)]

                    // we may need to deploy to multiple web servers, lets loop through each webserver and add a msdeploy step
                    webServerGroup.webServers.each { webServerName ->
                        webBuildSteps.add(new MsDeployStep(
                                name: "website-publish",
                                targetFolder: "${website.buildProject}.%VERSION%",
                                packageFolder: "%WORKSPACE%\\${website.buildProject}.%VERSION%",
                                deployWebsiteName: website.siteName,
                                deployServerName: webServerName,
                                appOffline: website.appOffline,
                                //deployTarget: "contentPath=%WORKSPACE$\\webdeploy",
                                environment: publishTransform,
                                //website.publishTransform,
                                //stack: stack,
                                //setParameters: website.setParameters
                                recycleBeforeDeploy: website.recycleBeforeDeploy,
                                setParameters: setParameters
                        )
                        )
                    }

                    new CiJobBuilder(
                            name: deployWebName,
                            cleanBeforeBuild: true,
                            buildToken: buildToken,
                            parameters: [
                                    new StringParam(
                                            name: "VERSION",
                                            defaultValue: "",
                                            description: "The version of the \"${website.siteName}\" website package to publish."
                                    )
                            ],
                            buildSteps: webBuildSteps
                    ).build(dslFactory)
                }
            }
        }
    }
}
