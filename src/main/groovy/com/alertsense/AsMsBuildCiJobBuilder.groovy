package com.alertsense

import com.alertsense.steps.BuildStep
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job
import utilities.AlertSense

/**
 *  Example Class for creating a Grails create
 */
class AsMsBuildCiJobBuilder {

    String name
    List<BuildStep> buildSteps
    String downStreamPublisher
    List<String> emails


    // Constants
    private def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
    private def msBuildArgs = '/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug'

    Job build(DslFactory dslFactory) {
        dslFactory.job(name) {
            it.description description

            if(workspace)
            {
                customWorkspace(workspace)
            }
            logRotator(60,-1,-1,-1)

            scm {
                git {
                    remote {
                        url(ownerAndProject)
                        credentials(credential)
                    }
                    branch(gitBranch)
                }
            }
            steps {
                buildSteps.each { step ->
                    configure AlertSense.step(step)
                }
                //msbuild("test","test","")
            }

            if(downStreamPublisher) {
                publishers {
                    downstream(downStreamPublisher, 'SUCCESS')
                }
            }
        }
    }
}
