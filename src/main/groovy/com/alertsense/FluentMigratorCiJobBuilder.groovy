package com.alertsense

import com.alertsense.config.Configuration
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 *  Example Class for creating a Grails create
 */
class FluentMigratorCiJobBuilder {

    Configuration config

    Job build(DslFactory dslFactory) {
        dslFactory.job("publish-${config.name}") {
            steps {
                config.buildSteps.each { step ->
                    step.create delegate
                }

            }
        }
    }

    /*

    String name
    String description
    String workspace
    String solutionDirectory
    String assembly
    String[] profiles
    String connection
    String downStreamPublisher;

    // Constants
    private def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
    private def args = '--provider sqlserver2012 --task migrate --verbose==true'

    // msbuild


    Job build(DslFactory dslFactory) {
        dslFactory.job(name) {
            it.description description

            if(workspace)
            {
                customWorkspace(workspace)
            }
            logRotator(60,-1,-1,-1)

            steps {
                batchFile(""" "${solutionDirectory}\\packages\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe" ${args} --assembly "${assembly}" --connectionString="${connection}" """.stripIndent().trim())

                // execute profiles if needed
                if(profiles) {
                    profiles.each {
                        batchFile(""" "${solutionDirectory}\\packages\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe" ${args} --assembly "${assembly}" --connectionString="${connection}" --profile=${it} """.stripIndent().trim())
                    }
                }
            }

            if(downStreamPublisher) {
                publishers {
                    downstream(downStreamPublisher, 'SUCCESS')
                }
            }

        }
    }*/
}
