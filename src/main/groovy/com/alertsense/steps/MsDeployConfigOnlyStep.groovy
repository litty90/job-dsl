package com.alertsense.steps

/**
 * Created by joey on 10/12/2015.
 */
class MsDeployConfigOnlyStep extends MsDeployStep {

    boolean appOffline = false
    boolean recycleBeforeDeploy = false


    protected def msDeployExe = "C:\\Program Files (x86)\\IIS\\Microsoft Web Deploy V3\\msdeploy.exe"

    @Override
    String getType() {
        return "MsDeployStep"
    }

    @Override
    void create(Object context) {

        context.with {

            def transformedConfig = "Web.config"

            def deployTarget =  "contentPath=${deployWebsiteName}/${transformedConfig},computername=${deployServerName}"

            def arguments = [
                    "-verb:sync",
                    "-source:${deployTarget}",
                    "-dest:${deployTarget}",
            ]

            if(setParameters) {
                setParameters.each { k, v ->
                    arguments.add("-setParam:kind=XmlFile,scope=${transformedConfig},value=\"${v}\",match=\"${k}\"")
                }
            }

            arguments.add("-verbose")

            batchFile("\"${msDeployExe}\" ${arguments.join(' ')}")
        }
    }
}
//msdeploy.exe