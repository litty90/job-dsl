package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class NugetPushStep extends NugetStepBase {
    String version
    String packageFolder

    boolean preRelease

    @Override
    String getType() {
        return "NugetPushStep"
    }

    @Override
    void create(context) {
        def outputFolder = "${targetFolder}\\${packageFolder}"

        nugetArgs.add("push \"${outputFolder}\\*.nupkg\"")

        if(nugetConfig.nugetApiKey) {
            nugetArgs.add("-apiKey ${nugetConfig.nugetApiKey}")
        }

        if(nugetConfig.nugetConfigPath) {
            nugetArgs.add("-ConfigFile \"${nugetConfig.nugetConfigPushPath}\"")
        }
        else {
            nugetArgs.add("-Source \"${nugetConfig.nugetPushSource}\"")
        }

        nugetArgs.add("-verbosity d")

        context.with {
            batchFile("\"${nugetConfig.nugetPath}\" ${nugetArgs.join(' ')}")
        }
    }
}
