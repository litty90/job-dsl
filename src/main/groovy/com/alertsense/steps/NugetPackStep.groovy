package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class NugetPackStep extends NugetStepBase {
    String version

    boolean preRelease
    boolean symbols = false

    @Override
    String getType() {
        return "NugetPackStep"
    }

    @Override
    void create(context) {
        def projectPath = "${targetFolder}\\${targetFile}"
        def outputFolder = "${targetFolder}\\obj"

        nugetArgs.add("pack ${projectPath}")
        nugetArgs.add("-OutputDirectory \"${outputFolder}\"")

        //nugetArgs.add("-IncludeReferencedProjects")

        if(symbols) {
            nugetArgs.add("-symbols")
        }

        if(version) {
            nugetArgs.add("-version ${version}")
        }

        nugetArgs.add("-verbosity d")

        context.with {
            batchFile("\"${nugetConfig.nugetPath}\" ${nugetArgs.join(' ')}")
        }
    }
}
