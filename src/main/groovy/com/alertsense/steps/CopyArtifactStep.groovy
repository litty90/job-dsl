package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class CopyArtifactStep extends BuildStep {
    String buildname
	String patterns
	boolean flattenDirectories = false

    @Override
    String getType() {
        return "CopyArtifactStep"
    }

    @Override
    void create(context) {
		context.with {
			copyArtifacts(buildname) {
				includePatterns(patterns)
				if(targetFolder) {
					targetDirectory(targetFolder)
				}
				buildSelector {
					latestSuccessful(true)
				}
				if(flattenDirectories) {
					flatten()
				}
			}
		}
    }
}
