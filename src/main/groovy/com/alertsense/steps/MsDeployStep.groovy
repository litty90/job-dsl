package com.alertsense.steps

/**
 * Created by joey on 10/12/2015.
 */
class MsDeployStep extends BuildStep {

    String packageFolder
    String environment
    String deployWebsiteName
    String deployServerName

    Map setParameters
    boolean appOffline = false
    boolean recycleBeforeDeploy = false


    protected def msDeployExe = "C:\\Program Files (x86)\\IIS\\Microsoft Web Deploy V3\\msdeploy.exe"

    @Override
    String getType() {
        return "MsDeployStep"
    }

    @Override
    void create(Object context) {

        context.with {

            def deployTarget =  "contentPath=${deployWebsiteName},computername=${deployServerName}"

            if(recycleBeforeDeploy) {
                batchFile("\"${msDeployExe}\" -verb:sync -source:recycleApp -dest=${deployTarget}")
            }

            def arguments = [
                    "-verb:sync",
                    "-source:contentPath=${packageFolder}",
                    "-dest:${deployTarget}",
                    "-skip:objectName=filePath,absolutePath=.*.nupkg",
            ]

            def transformedConfig = "Web.config"

            // in each project.groovy file we have a map of environment enums to web.config transform files. We use that map to determine which transformed file to use when deploying
            if (environment) {
                transformedConfig = "Web.${environment}.config.transformed"
                // we only need to replace web.config with a transformed file if it has been specified.
                arguments.add("-replace:objectName=filePath,match=${transformedConfig},replace=web.config")

            }

            if(setParameters) {
                setParameters.each { k, v ->
                    arguments.add("-setParam:kind=XmlFile,scope=${transformedConfig},value=\"${v}\",match=\"${k}\"")
                }
            }

            // publish with appOffline flag enabled so we can deploy over files locked by the application pool
            if(appOffline)
                arguments.add("-enableRule:AppOffline")

            arguments.add("-verbose")


            batchFile("\"${msDeployExe}\" ${arguments.join(' ')}")
        }
    }
}
//msdeploy.exe