package com.alertsense.steps

/**
 * Created by joey on 10/12/2015.
 */
class OctoBuild extends SolutionBuild {


    @Override
    String getType() {
        return "OctoBuild"
    }

    @Override
    void create(context) {

        //msBuildArgs.add("/t:Build")
        msBuildArgs.add("/p:RunOctoPack=true")
        //msBuildArgs.add("/p:OctoPackPublishPackageToFileShare=\"${targetFolder}\\bin\\octopack\"")

        super.create(context)

    }
}
