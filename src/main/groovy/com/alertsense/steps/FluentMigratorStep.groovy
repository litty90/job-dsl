package com.alertsense.steps
/**
 * Created by joey on 10/14/2015.
 */
class FluentMigratorStep extends BuildStep {
    String packageName
    String packageFolder
    String connectionString
    List<String> profiles = []

    List<String> arguments = ["--provider=sqlserver2012","--task=migrate","--verbose=true"]

    @Override
    String getType() {
        return "FluentMigratorStep"
    }

    @Override
    void create(context) {

        def assembly = "${packageFolder}\\${targetFolder}\\${targetFile}"
        // TODO pull this version from the project config
        def migrationTool = "${packageFolder}\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe"

        arguments.add("--target=\"${assembly}\"")
        arguments.add("--connectionString=\"${connectionString}\"")

        def command = "\"${migrationTool}\" ${arguments.join(' ')}"

        context.with {
            batchFile(command)

            profiles.each {
                batchFile("${command} --profile=${it}")
            }
        }
    }
}