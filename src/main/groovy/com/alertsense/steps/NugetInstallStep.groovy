package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class NugetInstallStep extends NugetStepBase {

    String packageName
    String packageFolder
    String version

    @Override
    String getType() {
        return "NugetInstallStep"
    }

    @Override
    void create(context) {

        nugetArgs.add("install ${packageName}")

        if(version)
            nugetArgs.add("-version ${version}")

        if(packageFolder) {
            nugetArgs.add("-OutputDirectory \"${packageFolder}\"")
        }

        if(nugetConfig.nugetConfigPath) {
            nugetArgs.add("-ConfigFile \"${nugetConfig.nugetConfigPath}\"")
        }
        else {
            nugetArgs.add("-Source \"${nugetConfig.nugetSource}\"")
        }

        nugetArgs.add("-verbosity d")

        context.with {
            batchFile("\"${nugetConfig.nugetPath}\" ${nugetArgs.join(' ')}")
        }
    }
}
