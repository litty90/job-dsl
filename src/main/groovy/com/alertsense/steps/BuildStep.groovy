package com.alertsense.steps

import com.alertsense.config.Configuration

/**
 * Created by joey on 10/12/2015.
 */
abstract class BuildStep {
    String name
    String targetFile
    String buildConfiguration
    String targetFolder
    String type

    Configuration configuration

    abstract void create(context)

    abstract String getType()
}
