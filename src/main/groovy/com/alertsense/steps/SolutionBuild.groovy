package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class SolutionBuild extends BuildStep {
    List<String> msBuildArgs = []
    String version
    boolean clean
    boolean rebuild

    protected def msBuildExe = 'C:\\Program Files (x86)\\MSBuild\\12.0\\Bin\\MSBuild.exe'

    @Override
    String getType() {
        return "SolutionBuild"
    }

    @Override
    void create(context) {

        if(clean)
            msBuildArgs.add("/t:Clean")
        if(rebuild)
            msBuildArgs.add("/t:Rebuild")

        if(version)
            msBuildArgs.add("/p:OctoPackPackageVersion=${version}")


        context.with {
            //configure AlertSense.MSBuild("MsBuild","test.sln","param")
            batchFile("\"${msBuildExe}\" \"${targetFolder}\\${targetFile}\" ${msBuildArgs.join(' ')}")
        }
    }



    def test(def context, String sMsBuildName, String sMsBuildFile, String sCmdLineArgs)  {

        println context.dump()

        context.stepNodes.add()

        /*context.gradle(tasks, options, true) { node ->
            node / wrapperScript('${NEBULA_HOME}/gradlew')
        }
            it / 'builders' << 'hudson.plugins.msbuild.MsBuildBuilder' {
                msBuildName sMsBuildName
                msBuildFile sMsBuildFile
                cmdLineArgs sCmdLineArgs
                buildVariablesAsProperties true
                continueOnBuildFailure false
            }
        }*/
    }
}
