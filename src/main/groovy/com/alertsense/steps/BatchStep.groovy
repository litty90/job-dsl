package com.alertsense.steps

import com.alertsense.config.Configuration

/**
 * Created by joey on 10/12/2015.
 */
class BatchStep extends BuildStep {

    String command

    @Override
    String getType() {
        return "BatchStep"
    }

    @Override
    void create(Object context) {

        context.with {
            batchFile(command)
        }
    }
}
