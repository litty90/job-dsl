package com.alertsense.steps

import com.alertsense.config.NugetConfig

/**
 * Created by joey on 10/12/2015.
 */
class NugetStepBase extends BuildStep {
    NugetConfig nugetConfig
    List<String> nugetArgs = []

    @Override
    String getType() {
        return "NugetStepBase"
    }

    @Override
    void create(context) {

    }
}
