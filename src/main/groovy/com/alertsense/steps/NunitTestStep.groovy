package com.alertsense.steps

import com.alertsense.config.TestProjectConfiguration

/**
 * Created by joey on 10/12/2015.
 */
class NunitTestStep extends BuildStep {
    TestProjectConfiguration testConfig

    List<String> nunintArgs = []

    //private String nunitConsoleRunnerPath =  "C:\\Program Files (x86)\\NUnit 2.6.4\\bin\\nunit-console.exe"
    private String nunitConsoleRunnerPath =  "C:\\Program Files (x86)\\NUnit 2.6.4\\bin\\nunit-console-x86.exe"

    @Override
    String getType() {
        return "NunitTestStep"
    }

    @Override
    void create(context) {

        nunintArgs.add("\"${nunitConsoleRunnerPath}\"")
        nunintArgs.add(testConfig.nunitTestFile)
        nunintArgs.add("/nologo")
        nunintArgs.add("/labels")
        nunintArgs.add("/framework=4.5")
        nunintArgs.add("/process=Multiple")

        if(testConfig.excludedCategories) {
            nunintArgs.add("/exclude:${testConfig.excludedCategories.join(',')}")
        }
        if(testConfig.includeCategories) {
            nunintArgs.add("/include:${testConfig.includeCategories.join(',')}")
        }

        nunintArgs.add("/xml=Nunit.${testConfig.resultsLabel}.xml")


        context.with {
            batchFile(nunintArgs.join(' '))
        }
    }
}
