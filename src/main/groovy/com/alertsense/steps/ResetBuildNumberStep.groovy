package com.alertsense.steps

/**
 * Created by joey on 11/17/2015.
 */
class ResetBuildNumberStep extends BuildStep {

    String buildName

    @Override
    String getType() {
        return "BatchStep"
    }

    @Override
    void create(Object context) {

        String script = """ import jenkins.model.*
        import hudson.model.*

        def buildNumber = build.buildVariableResolver.resolve("BUILDNUMBER")

        item = Jenkins.instance.getItemByFullName("${buildName}")
        item.getBuilds().each { it.delete() }
        item.updateNextBuildNumber(buildNumber.toInteger())"""

        context.with {
            systemGroovyCommand(script)
        }
    }
}