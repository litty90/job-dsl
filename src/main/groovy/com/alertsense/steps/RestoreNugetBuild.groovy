package com.alertsense.steps
/**
 * Created by joey on 10/12/2015.
 */
class RestoreNugetBuild extends BuildStep {
    String nugetPath
    String nugetConfigPath

    @Override
    String getType() {
        return "NugetPackStep"
    }

    @Override
    void create(context) {

        context.with {
            batchFile("\"${nugetPath}\" restore \"${targetFolder}\\${targetFile}\" -ConfigFile \"${nugetConfigPath}\"")
        }
    }
}
