package com.alertsense.steps
/**
 * Created by joey on 10/14/2015.
 */
class EntityFrameworkMigratorStep extends BuildStep {
    String packageName
    String packageFolder
    String connectionString
    List<String> profiles = []

    List<String> arguments = []

    @Override
    String getType() {
        return "EntityFrameworkMigratorStep"
    }

    @Override
    void create(context) {

        def assemblyDirectory = "${packageFolder}\\${targetFolder}"
        // TODO pull this version from the project config
        def migrationTool = "${packageFolder}\\EntityFramework.4.3.1\\tools\\Migrate.exe"

        arguments.add("${targetFile}")
        arguments.add("/connectionProviderName=\"System.Data.SqlClient\"")
        arguments.add("/connectionString=\"${connectionString}\"")
        arguments.add("/StartupDirectory=\"${assemblyDirectory}\"")

        def command = "\"${migrationTool}\" ${arguments.join(' ')}"

        context.with {
            batchFile(command)
        }
    }

}