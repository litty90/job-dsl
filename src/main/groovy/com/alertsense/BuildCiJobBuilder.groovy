package com.alertsense

import com.alertsense.config.Configuration
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 * Example Class for creating a Gradle create
 */





class BuildCiJobBuilder {

    Configuration config

    Job build(DslFactory dslFactory) {
        dslFactory.job("build-${config.name}") {
            it.description "Build job for ${config.name}"
            scm {
                git {
                    remote {
                        url config.gitRepository
                    }
                    branch(config.gitBranch)
                }
            }
            steps {
                config.buildSteps.each { step ->
                    step.create delegate
                }

            }
        }
    }
}
