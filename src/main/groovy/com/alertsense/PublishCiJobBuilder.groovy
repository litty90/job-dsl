package com.alertsense

import com.alertsense.config.Configuration
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

/**
 * Example Class for creating a Gradle create
 */
class PublishCiJobBuilder {

    Configuration config

    Job build(DslFactory dslFactory) {
        dslFactory.job("publish-${config.name}") {
            steps {
                config.buildSteps.each { step ->
                    step.create delegate
                }

            }
        }
    }
}
