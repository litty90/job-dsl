package com.alertsense.config

/**
 * Created by joey on 10/14/2015.
 */
class StackConfig {
    String name
    String databaseServer
    String elastiCacheHost
    Map smsShortCodes
    String devOpsEmailAddress = "dev_devops@alertsense.com"
    MqServerConfig mqServerConfig
    EnvironmentType environmentType
    WebServerGroupConfig[] webServerGroups

    BlobStorageConfig blobStorage

    boolean ignoreSetParameters = false
    def hostUri
    def setHostUri(Closure closure) {
        println "setting closure"
        hostUri = closure
        hostUri.delegate = this
    }

}


