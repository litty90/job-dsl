package com.alertsense.config

/**
 * Created by joey on 10/18/2015.
 */
class WebsiteConfiguration {
    String buildProject
    String siteName
    Map publishTransform
    boolean appOffline = false
    boolean recycleBeforeDeploy = false
    def parametersClosure
}
