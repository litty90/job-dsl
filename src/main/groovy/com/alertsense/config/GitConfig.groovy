package com.alertsense.config

/**
 * Created by joey on 10/14/2015.
 */
class GitConfig {
    String gitRepository
    String gitBranch
    String gitCredentialId
	boolean clean = true

    boolean gitTagBuild = false
    String gitTagVersion
}


