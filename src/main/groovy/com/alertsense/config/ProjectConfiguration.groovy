package com.alertsense.config

/**
 * Created by joey on 10/8/2015.
 */
class ProjectConfiguration {
    String projectName
    String projectFile
    String projectFolder
    boolean publishNuget = false
}
