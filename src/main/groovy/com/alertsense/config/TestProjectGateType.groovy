package com.alertsense.config

/**
 * Created by joey on 10/26/2015.
 */
enum TestProjectGateType {
    Build,
    Integration
}
