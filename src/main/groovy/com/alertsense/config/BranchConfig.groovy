package com.alertsense.config

/**
 * Created by joey on 10/28/2015.
 */
class BranchConfig {
    String branchName
    String version
    String prefix

    String getPackageVersion() {
        def versionStr = "$version"
        if(prefix)
            versionStr += "-${prefix}%BUILD_NUMBER%"
        else
            versionStr += ".%BUILD_NUMBER%"
        return versionStr
    }
}
