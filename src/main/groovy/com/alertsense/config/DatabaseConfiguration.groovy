package com.alertsense.config

/**
 * Created by joey on 10/18/2015.
 */
class DatabaseConfiguration {
    String buildProject
    String databaseName
    List<String> profiles = []
    DatabaseMigrator migrationType = DatabaseMigrator.FluentMigrator
}


