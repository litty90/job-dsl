package com.alertsense.config

/**
 * Created by joey on 10/26/2015.
 */
class TestProjectConfiguration {
    String nunitTestFile
    String resultsLabel = "unit-test"
    List<String> excludedCategories
    List<String> includeCategories
    TestProjectGateType testType
}
