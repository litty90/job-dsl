package com.alertsense.config
/**
* Created by joey on 10/18/2015.
*/
class ConfigurationFactory {
        static NugetConfig getNugetConfig() {
                return getNugetConfig(NugetSource.AlertSenseBuild)
        }
        static NugetConfig getNugetConfig(NugetSource nugetSource) {

                switch (nugetSource) {
                        case NugetSource.Dovetail:
                                return new NugetConfig(
                                nugetPath: "C:\\ProgramData\\Nuget\\nuget.exe",
                                nugetApiKey: "lEOVoPzUAcVhbjEw20brV/qe7bbDJLW9XoaxhmBSWN6ZPr7/ydmocF6BA0zaAqueCe/UgADaq+rFfrEucF402A==",
                                nugetSource: "http://dovetail.alertsense.com/nuget;https://www.nuget.org/api/v2",
                                nugetPushSource: "http://dovetail.alertsense.com/api/v2/package/",
                                )
                        case NugetSource.AlertSenseBuild:
                                return new NugetConfig(
                                nugetPath:"C:\\ProgramData\\Nuget\\nuget.exe",
                                nugetApiKey: "78857502-585b-4d83-b07e-6d2441c3c592",
                                nugetSource: "https://www.myget.org/F/alertsense-deploy/auth/78857502-585b-4d83-b07e-6d2441c3c592/api/v2;http://dovetail.alertsense.com/nuget;https://www.nuget.org/api/v2",
                                nugetPushSource: "https://www.myget.org/F/alertsense-deploy/api/v2/package",
                                )
                }
        }



        static List<StackConfig> getStackConfig() {

                def productionBlobStorage = new BlobStorageConfig(
                        name: "alertsenseadmin",
                        key: "XMsv12nQz172D+TKci+x8ykvgpi/MQsj9q6+47jSxG0JweKIsZi/BfsI646LuGwRI+pkHOMsNy0bN2oR7UCYng==",
                        ttsName: "alertsensefiles",
                        ttsKey: "aFCtA9K0TMtFJtPEIA+CGllrdlSQZUCGJS3uW1v5vBeYB0I1cE2YsVi9dcbg0H195wv66wfnQkpWDCnmGl9BjA==",

                )

                def stagingBlobStorage = new BlobStorageConfig(
                        name: "alertsenseadminstaging",
                        key: "f9YEOsJoa69vXGfVSIjkfWRkcB1InHdQZ/5XiypSHcO/YExEKVUQzPH5zPP7by8aHGFNQDMjrDEPGBGiXtRcew==",
                        ttsName: "alertsensefiles",
                        ttsKey: "aFCtA9K0TMtFJtPEIA+CGllrdlSQZUCGJS3uW1v5vBeYB0I1cE2YsVi9dcbg0H195wv66wfnQkpWDCnmGl9BjA==",
                )

                def safeBlobStorage = new BlobStorageConfig(
                        name: "alertsenseadmintest",
                        key: "zBtXDyrAEd3CE7XASuf2WVbAU/Mvjpw/c1Fgn11sOd9gdqY54iLSc1/31+XxakO7hJ8ONfZLVGV8ICsLr3CWXQ==",
                        ttsName: "alertsensefiles",
                        ttsKey: "aFCtA9K0TMtFJtPEIA+CGllrdlSQZUCGJS3uW1v5vBeYB0I1cE2YsVi9dcbg0H195wv66wfnQkpWDCnmGl9BjA==",
                )

                def awsPrdocutionShortCodes = ["US": "38276", "CA": "80846"]
                def awsTestShortCodes = ["US": "12085495486", "CA": "80846"]

                def elastiCache = "wes-ca-ynl5i95pz6fl.nsrsdt.cfg.usw2.cache.amazonaws.com"

                def awsTestHostUri = { String scheme, String service, String path ->

                        println "the url https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"

                        if(scheme == "https")
                                return "https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"
                        else
                                return "http://${service}-${delegate.name}.aws.alertsense.net${path}"

                }

                def awsProdHostUri = awsTestHostUri
                def drHostUri = awsTestHostUri


                // When creating new StackConfig entries for each EnvironmentType, mqServerConfig should point to a unique cloudamqp stack.
                // That or something installed on localhost.....which should never be the case for any stack running in aws.
                return [
                        new StackConfig(
                                name: "dr",
                                ignoreSetParameters: true,
                                environmentType: EnvironmentType.Dr,
                                databaseServer: "dr-aw-db01",
                                smsShortCodes: awsPrdocutionShortCodes,
                                webServerGroups:[
                                        new WebServerGroupConfig(
                                                name: "dr",
                                                webServers: ["dr-aw-hk01"]
                                        )
                                ],
                                elastiCacheHost: elastiCache,
                                mqServerConfig: new MqServerConfig(
                                        connectionString: "amqp://localhost:5671/",
                                        username: "guest",
                                        password: "guest",
                                        useSsl: false
                                ),
                                hostUri: { String scheme, String service, String path ->
                                        if(scheme == "https")
                                                return "https://dr-${service}.alertsense.com${path}"
                                        else
                                                return "http://dr-${service}.alertsense.com${path}"
                                },
                                blobStorage: productionBlobStorage,

                        ),
                        new StackConfig(
                                name: "west",
                                environmentType: EnvironmentType.Production,
                                databaseServer: "db",
                                smsShortCodes: awsTestShortCodes,
                                webServerGroups: [
                                        new WebServerGroupConfig(
                                                name: "left",
                                                webServers: ["web1","web2"]
                                        ),
                                        new WebServerGroupConfig(
                                                name: "right",
                                                webServers: ["web3","web4"]
                                        ),
                                ],
                                elastiCacheHost: "wes-ca-1mqx15ji8vj1o.nsrsdt.cfg.usw2.cache.amazonaws.com",
                                mqServerConfig: new MqServerConfig(
                                        connectionString: "amqp://silver-reindeer.rmq.cloudamqp.com:5671/kigcrrso",
                                        username: "kigcrrso",
                                        password: "vcq8lP9ME2wNivEVvyzdrWSq9WPPx1yb"
                                ),
                                hostUri: { String scheme, String service, String path ->
                                        if(scheme == "https")
                                                return "https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"
                                        else
                                                return "http://${service}-${delegate.name}.aws.alertsense.net${path}"
                                },
                                blobStorage: productionBlobStorage,
                        ),
                        new StackConfig(
                                name: "east",
                                environmentType: EnvironmentType.Production,
                                databaseServer: "db",
                                smsShortCodes: awsTestShortCodes,
                                webServerGroups: [
                                        new WebServerGroupConfig(
                                                name: "left",
                                                webServers: ["web1","web2"]
                                        ),
                                        new WebServerGroupConfig(
                                                name: "right",
                                                webServers: ["web3","web4"]
                                        ),
                                ],
                                // this elasticache cluster is in the east region. you have to change your dropdown to discover it
                                elastiCacheHost: "eas-ca-eh95y4nkmjbg.dvhzea.cfg.use1.cache.amazonaws.com",
                                mqServerConfig: new MqServerConfig(
                                        connectionString: "amqp://hyena.rmq.cloudamqp.com:5671/rbzzkfxu",
                                        username: "rbzzkfxu",
                                        password: "nNenWEBvwOYXyvsG_nYyZBdvGd85rrzW"
                                ),
                                hostUri: { String scheme, String service, String path ->
                                        if(scheme == "https")
                                                return "https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"
                                        else
                                                return "http://${service}-${delegate.name}.aws.alertsense.net${path}"
                                },
                                blobStorage: productionBlobStorage,
                        ),
                        new StackConfig(
                                name: "staging",
                                environmentType: EnvironmentType.Safe,
                                databaseServer: "db",
                                smsShortCodes: awsTestShortCodes,
                                webServerGroups: [
                                        new WebServerGroupConfig(
                                                name: "left",
                                                webServers: ["web1","web2"]
                                        ),
                                        new WebServerGroupConfig(
                                                name: "right",
                                                webServers: ["web3","web4"]
                                        ),
                                ],
                                elastiCacheHost: "sta-ca-12tuskdessjeu.dvhzea.cfg.use1.cache.amazonaws.com",
                                mqServerConfig: new MqServerConfig(
                                        connectionString: "amqp://jaguar.rmq.cloudamqp.com:5671/zvedxlgn",
                                        username: "zvedxlgn",
                                        password: "qrSe0J8dYoHQvZRH9OWLYSOJEwHp_4Mb"
                                ),
                                hostUri: { String scheme, String service, String path ->
                                        if(scheme == "https")
                                                return "https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"
                                        else
                                                return "http://${service}-${delegate.name}.aws.alertsense.net${path}"
                                },
                                blobStorage: stagingBlobStorage,
                        ),
                        new StackConfig(
                                name: "safe",
                                devOpsEmailAddress: "josiah.peters@alertsense.com,grady.cooper@alertsense.com",
                                environmentType: EnvironmentType.Safe,
                                databaseServer: "db",
                                smsShortCodes: awsTestShortCodes,
                                webServerGroups: [
                                        new WebServerGroupConfig(
                                                name: "main",
                                                webServers: ["web1"]
                                        ),
                                ],
                                elastiCacheHost: "cache-safe.aws.alertsense.net",
                                mqServerConfig: new MqServerConfig(
                                        connectionString: "amqp://hyena.rmq.cloudamqp.com:5671/bhtfysdi",
                                        username: "bhtfysdi",
                                        password: "wd6i2dY9eR7btR08K4Pz62TEy5dq7aQN"
                                ),
                                hostUri: { String scheme, String service, String path ->
                                        if(scheme == "https")
                                                return "https://${service}-${delegate.name}.aws.alertsense.net:8443${path}"
                                        else
                                                return "http://${service}-${delegate.name}.aws.alertsense.net${path}"

                                },
                                blobStorage: safeBlobStorage,
                        )
                ]
        }
}
