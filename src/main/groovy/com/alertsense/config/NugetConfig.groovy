package com.alertsense.config

/**
 * Created by joey on 10/13/2015.
 */
class NugetConfig {
    String nugetPath
    String nugetSource
    String nugetPushSource
    String nugetConfigPath
    String nugetConfigPushPath
    String nugetApiKey
}
