package com.alertsense.config

/**
 * Created by joey on 10/19/2015.
 */
enum DatabaseMigrator {
    FluentMigrator,
    EntityFrameworkMigration
}
