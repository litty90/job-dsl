package com.alertsense.config

/**
 * Created by joey on 10/27/2015.
 */
class MqServerConfig {
    String connectionString
    String username
    String password
    boolean publisherConfirms = true
    boolean useSsl = true

}
