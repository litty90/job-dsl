package com.alertsense.config

import com.alertsense.steps.BuildStep


class Configuration {
    String name
    String repoName
    String repoPath
    String solutionDirectory
    String solutionFile
    String baseDirectory
    String gitRepository
    String gitCredentialId
    String connectionString
    String databaseName
    String websiteName

    String nugetSolutionPath = "C:\\ProgramData\\Nuget\\nuget.exe"
    String nugetSolutionConfigPath = ".\\src\\.nuget\\Nuget.config"

    NugetConfig nugetConfig

    List<BuildStep> buildSteps = []


    HashMap<String,ProjectConfiguration> buildProjects = []
    List<DatabaseConfiguration> databases = []
    List<WebsiteConfiguration> websites = []
    List<TestProjectConfiguration> tests = []
    BranchConfig[] branches
    List<StackConfig> stacks = []


    List<TestProjectConfiguration> getPostBuildTests()
    {
        List<TestProjectConfiguration> testProjects = []

        tests.each { testConfig ->
            if(testConfig.testType == TestProjectGateType.Build)
            {
                testProjects.add(testConfig)
            }
        }

        if(testProjects.size() > 0)
            return testProjects
        else
            return null
    }

}


