package com.alertsense.config

/**
 * Created by josia on 11/3/2015.
 */
enum EnvironmentType {
    Production,
    Integration,
    Safe, // pergo
    Dr, // disaster recovery
    DrIntegration // disaster recovery warm standby
}
