package com.alertsense

import groovy.json.JsonOutput
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job
import groovy.json.StringEscapeUtils;
/**
 *  Example Class for creating a Grails create
 */
class MsDeployCiJobBuilder {


    Job build(DslFactory dslFactory) {
        dslFactory.job("msdeploy-${config.name}") {
            steps {
                config.buildSteps.each { step ->
                    step.create delegate
                }

            }
        }
    }
}
