package utilities

import com.alertsense.steps.NugetPackStep
import com.alertsense.steps.OctoBuild

class AlertSense 
{
	static Closure NunitTest(String sTestResultsPattern)  {
        return {
                it / 'publishers' << 'hudson.plugins.nunit.NUnitPublisher' {
                        testResultsPattern sTestResultsPattern
                        debug false
                        keepJUnitReports false
                        skipJUnitArchiver false
                        failIfNoResults true
                }
        }
    }
	static Closure MSBuild(String sMsBuildName, String sMsBuildFile, String sCmdLineArgs)  {


        return {
            println it.dump()
            println this.dump()
            println owner.dump()
            println "msbuild"

            it / 'builders' << 'hudson.plugins.msbuild.MsBuildBuilder' {
                        msBuildName sMsBuildName
                        msBuildFile sMsBuildFile
                        cmdLineArgs sCmdLineArgs
                        buildVariablesAsProperties true
                        continueOnBuildFailure false
                }
        }
    }
	static Closure Batch(String sCommand)  {
        return {
                it / 'builders' << 'hudson.tasks.BatchFile' {
                        command sCommand
                }
        }
    }

    static Closure PowerShell(String sCommand)  {
        return {
                it / 'builders' << 'hudson.plugins.powershell.PowerShell' {
                        command sCommand
                }
        }
    } 
	static Closure NunitReports(String sTestResultsFile) {
        return { 
            it / 'publishers' << 'hudson.plugins.mstest.MSTestPublisher' {
                  testResultsFile sTestResultsFile
           }         
        }
    }

    static Closure buildStep(NugetPackStep nupgkBuild) {

        def projectPath = "${nupgkBuild.targetFolder}\\${nupgkBuild.targetFile}"
        def outputFolder = "${nupgkBuild.targetFolder}\\bin\\nuget"

        println(nupgkBuild)

        return {
            batchFile("nuget pack ${projectPath} -OutputDirectory \"\${outputFolder}\" -symbols")
        }
    }

    static Closure buildStep(OctoBuild octoBuild) {

        def projectPath = "${octoBuild.targetFolder}\\${octoBuild.targetFile}"
        def outputFolder = "${octoBuild.targetFolder}\\bin\\nuget"

        return {
            batchFile("octobuild")
        }
    }
}	