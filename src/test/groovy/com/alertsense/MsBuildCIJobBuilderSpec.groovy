package com.alertsense

import com.alertsense.steps.NugetPackStep
import com.alertsense.steps.OctoBuild
import groovy.json.JsonOutput
import javaposse.jobdsl.dsl.Job
import javaposse.jobdsl.dsl.JobManagement
import javaposse.jobdsl.dsl.JobParent
import spock.lang.Specification

@Mixin(JobSpecMixin)
class MsBuildCIJobBuilderSpec extends Specification {

    JobParent jobParent = createJobParent()

    AsMsBuildCiJobBuilder builder

    def setup() {
        JobManagement jm = JobSpecHelper.getJobManagement(parameters)

        jobParent.setJm(jm)

        builder = new AsMsBuildCiJobBuilder(
                name: jobName,
                buildSteps: [ new NugetPackStep(name:"MsBuild"),
                                new OctoBuild(name:"nugetpkg")],
                downStreamPublisher:  nextJob
        )

    }

    void 'test XML output'() {
        when:
        Job job = builder.build(jobParent)

        println(JsonOutput.toJson(builder.buildSteps))

        println(job.getXml())
        //jobParent.file

        then:
        true
    }
}
