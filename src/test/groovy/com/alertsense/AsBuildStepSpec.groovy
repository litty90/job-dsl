package com.alertsense

import com.alertsense.steps.BuildStep
import com.alertsense.steps.DacPacBuild
import com.alertsense.steps.NugetPackStep
import com.alertsense.steps.OctoBuild
import com.alertsense.steps.MsDeployStep
import groovy.json.JsonOutput
import spock.lang.Specification

//@Mixin(JobSpecMixin)
class AsBuildStepSpec extends Specification {

    String json
    List<BuildStep> expectedBuildSteps
    List<BuildStep> actualBuildSteps

    def setup() {
        expectedBuildSteps = [
                new DacPacBuild(name:  "dacpac"),
                new NugetPackStep(name:"nugetpkg"),
                new MsDeployStep(name:"webdeploy"),
                new OctoBuild(name:"octo"),
        ]

        json = JsonOutput.toJson(expectedBuildSteps)
    }

    void 'test XML output'() {
        when:
            actualBuildSteps = BuildStep.ParseFromJson(json)

        then:

        for (int i = 0; i < expectedBuildSteps.size() ; i++) {
            def expectedProperties = expectedBuildSteps[i].getProperties()
            // println(expectedProperties)
            def actualProperties = actualBuildSteps[i].getProperties()
            // println(actualProperties)

            assert expectedProperties == actualProperties
        }
    }
}
