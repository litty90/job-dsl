package com.alertsense

import com.alertsense.config.Configuration
import com.alertsense.config.NugetConfig
import com.alertsense.steps.*
import groovy.io.FileType
import javaposse.jobdsl.dsl.JobManagement
import javaposse.jobdsl.dsl.MemoryJobManagement

/**
 * Created by joey on 10/8/2015.
 */
class JobSpecHelper {
    static JobManagement getJobManagement( Map<String, String> parameters) {
        JobManagement jm = new MemoryJobManagement()

        parameters.each { k, v ->
            println("${k}:${v}")
            jm.parameters.put(k,v)
        }

        new File('resources').eachFileRecurse(FileType.FILES) {
            def fileIndex = it.path.replaceAll('\\\\', '/')
            jm.availableFiles[fileIndex] = it.text
            println(fileIndex)
        }

        jm
    }

    static NugetConfig getNugetConfig(String repoPath)
    {
        def config = new NugetConfig (
            nugetPath: "${repoPath}\\src\\.nuget\\nuget.exe",

                //nugetConfigPath: "${repoPath}\\src\\.nuget\\dovetail.pull.config",
                //nugetConfigPushPath: "${repoPath}\\src\\.nuget\\dovetail.push.config"

                nugetConfigPath: "${repoPath}\\src\\.nuget\\alertsense-build.pull.config",
                nugetConfigPushPath: "${repoPath}\\src\\.nuget\\alertsense-build.push.config"


        )

        return config
    }

    static Configuration getHickoryConfiguration()
    {
        def repoPath = "."
        def config = new Configuration(
                name: "Hickory",
                repoPath: repoPath,
                solutionFile: "src\\AlertSense.Hickory.sln",
                baseDirectory: "${repoPath}\\src",
                gitRepository: "git@bitbucket.org:AlertSense/Hickory.git",
                gitBranch: "dev",
                gitCredentialId: "19f4b472-3a57-46b6-ad4b-0f6441867910",
                connectionString: "Data Source=(localdb)\\projects;Database=local-hickory;Integrated Security=True"
        )

        return config
    }

    static Configuration getHickoryMsDeployConfiguration()
    {
        def version = "2.17.0.26959"

        def config = getHickoryConfiguration()
        def nugetConfig = getNugetConfig(config.repoPath)

        config.buildSteps = [
                new NugetInstallStep(
                        name: "install-package",
                        nugetConfig: nugetConfig,
                        packageName: "AlertSense.Hickory.AdminWebsite",
                        packageFolder: "${config.repoPath}\\deploy-packages",
                        version: version
                ),
                new MsDeployStep(
                        name: "database",
                        targetFolder: "AlertSense.Hickory.AdminWebsite.${version}",
                        packageFolder:  "\${WORKSPACE}\\deploy-packages",
                        deployTarget: "contentPath=\${WORKSPACE}\\webdeploy",
                        environment: "prod-admin"
                )
        ]
        return config
    }

    static Configuration getHickoryFluentMigratorConfiguration()
    {
        def version = "2.17.0.26953"

        def config = getHickoryConfiguration()
        def nugetConfig = getNugetConfig(config.repoPath)

        config.buildSteps = [
                new NugetInstallStep(
                        name: "install-package",
                        nugetConfig: nugetConfig,
                        packageName: "AlertSense.Hickory.Data.Setup",
                        packageFolder:  "${config.repoPath}\\deploy-packages",
                        version: version
                ),
                new FluentMigratorStep(
                        name: "database",
                        targetFile: "AlertSense.Hickory.Data.Setup.dll",
                        targetFolder: "AlertSense.Hickory.Data.Setup.${version}\\lib\\net45",
                        packageFolder: "${config.repoPath}\\deploy-packages",
                        connectionString: config.connectionString,
                        profiles: ["static", "Development"]
                )
        ]
        return config
    }

    static Configuration getHickoryPublishConfiguration()
    {
        def config = getHickoryConfiguration()



        def nugetConfig = getNugetConfig(config.repoPath)

        config.buildSteps = [
                new NugetPushStep(
                        name: "website",
                        nugetConfig: nugetConfig,
                        targetFolder: "${config.baseDirectory}\\App\\AlertSense.Hickory.AdminWebsite",
                        packageFolder: "obj\\octopacked",
                ),
                new NugetPushStep(
                        name: "database",
                        nugetConfig: nugetConfig,
                        targetFolder: "${config.baseDirectory}\\Dal\\AlertSense.Hickory.Data.Setup",
                        packageFolder: "bin\\nuget",
                )
        ]
        return config
    }

    static Configuration getHickoryBuildConfiguration()
    {
        def repoPath = "."
        def nugetPath = "${repoPath}\\src\\.nuget\\nuget.exe"

        def config = getHickoryConfiguration()
        def nugetConfig = getNugetConfig(config.repoPath)


        config.buildSteps = [
            new RestoreNugetBuild(
                name: "restore-package",
                nugetConfig: nugetConfig,
                targetFolder: "${config.baseDirectory}",
                targetFile: "AlertSense.Hickory.sln",
            ),
            new SolutionBuild(
                name: "solution-create",
                rebuild: true,
                clean: true,
                targetFolder: "${config.baseDirectory}",
                targetFile: "AlertSense.Hickory.sln",
            ),
            new OctoBuild(
                name: "admin-website",
                rebuild: false,
                clean: false,
                targetFolder: "${config.baseDirectory}\\App\\AlertSense.Hickory.AdminWebsite",
                targetFile: "AlertSense.Hickory.AdminWebsite.csproj"
            ),
            new OctoBuild(
                name: "admin-website",
                rebuild: false,
                clean: false,
                targetFolder: "${config.baseDirectory}\\App\\AlertSense.Hickory.PublicWeb",
                targetFile: "AlertSense.Hickory.PublicWeb.csproj"
            ),
            new NugetPackStep(
                name: "database",
                nugetConfig: nugetConfig,
                targetFolder: "${config.baseDirectory}\\Dal\\AlertSense.Hickory.Data.Setup",
                targetFile: "AlertSense.Hickory.Data.Setup.csproj",
            )
        ]
        return config
    }
}
