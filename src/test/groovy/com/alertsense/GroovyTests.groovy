package com.alertsense

import com.alertsense.steps.*
import groovy.json.JsonOutput
import spock.lang.Specification

//@Mixin(JobSpecMixin)
class GroovyTests extends Specification {


    def tester = new Helper()

    def result = ""

    void 'test XML output'() {
        when:

            def cloj = { String test ->
                return "hello ${test}."
            }

        tester.method = cloj
        result = tester.execute("Name")

        then:
            assert result == "hello Name."
    }
}

class Helper {

    def method

    String execute(String name) {
        return method(name)
    }

}