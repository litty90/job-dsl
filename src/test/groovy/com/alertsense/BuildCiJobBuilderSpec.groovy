package com.alertsense

import com.alertsense.config.Configuration
import javaposse.jobdsl.dsl.Job
import javaposse.jobdsl.dsl.JobParent
import spock.lang.Specification

@Mixin(JobSpecMixin)
class BuildCiJobBuilderSpec extends Specification {

    JobParent jobParent = createJobParent()

    Configuration config
    BuildCiJobBuilder builder

    def setup() {
        //config = Configuration.getFromJsonFile("resources/hickory.json")

        def config = JobSpecHelper.getHickoryBuildConfiguration()

        builder = new BuildCiJobBuilder(config: config)
    }

    void 'test XML output'() {
        when:
        Job job = builder.build(jobParent)

        then:

        println(job.getXml())
    }
}
