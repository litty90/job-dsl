package com.alertsense

import com.alertsense.config.Configuration
import javaposse.jobdsl.dsl.JobParent
import spock.lang.Specification

@Mixin(JobSpecMixin)
class CIPipelineBuilderSpec extends Specification {

    JobParent jobParent = createJobParent()

    Configuration config

    BuildCiJobBuilder buildJobBuilder
    PublishCiJobBuilder publishJobBuilder
    FluentMigratorCiJobBuilder migrateJobBuilder
    MsDeployCiJobBuilder deployJobBuilder

    def setup() {
        //config = Configuration.getFromJsonFile("resources/hickory.json")

        buildJobBuilder = new BuildCiJobBuilder(config: JobSpecHelper.getHickoryBuildConfiguration())
        publishJobBuilder = new PublishCiJobBuilder(config: JobSpecHelper.getHickoryPublishConfiguration())
        migrateJobBuilder = new FluentMigratorCiJobBuilder(config: JobSpecHelper.getHickoryFluentMigratorConfiguration())
        deployJobBuilder = new MsDeployCiJobBuilder(config: JobSpecHelper.getHickoryMsDeployConfiguration())

    }

    void 'test XML output'() {
        when:
        def buildJob = buildJobBuilder.build(jobParent)
        def publishJob = publishJobBuilder.build(jobParent)
        def migrateJob = migrateJobBuilder.build(jobParent)
        def deployJob = deployJobBuilder.build(jobParent)

        then:

        println(buildJob.getXml())
        println(publishJob.getXml())
        println(migrateJob.getXml())
        println(deployJob.getXml())
    }
}
