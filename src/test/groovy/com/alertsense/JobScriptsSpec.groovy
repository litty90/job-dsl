package com.alertsense

import groovy.io.FileType
import javaposse.jobdsl.dsl.DslScriptLoader
import javaposse.jobdsl.dsl.JobManagement
import javaposse.jobdsl.dsl.MemoryJobManagement
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Tests that all dsl scripts in the jobs directory will compile.
 */
class JobScriptsSpec extends Specification {

    @Unroll
    void 'test script #file.name'(File file) {
        given:
        JobManagement jm = new MemoryJobManagement()

        jm.parameters.put("stack","east")
        jm.parameters.put("dbServer","prod")
        jm.parameters.put("webServer","web1")
        jm.parameters.put("gitBranch","dev")

        new File('resources').eachFileRecurse(FileType.FILES) {
            jm.availableFiles[it.path.replaceAll('\\\\', '/')] = it.text
        }

        when:
        def foo = DslScriptLoader.runDslEngine file.text, jm

        jm.savedConfigs.eachWithIndex  { item, index ->
            /*File f =  new File("config-$file.name" + index)

            if(f.exists()) {
                f.delete()
            }

            f.createNewFile();

            f << item*/

            println item
        }

        foo = null
        then:
        noExceptionThrown()

        where:
        file << jobFiles
    }

    static List<File> getJobFiles() {
        List<File> files = []
        new File('jobs/alertsense').eachFileRecurse(FileType.FILES) {

            if (it.name.endsWith('storage.groovy')) {
                files << it
            }
            files << it

            /*if (it.name.endsWith('hickory.groovy')) {
                files << it
            }
            /*
            if (it.name.endsWith('elm.groovy')) {
                files << it
            }
            if (it.name.endsWith('bamboo.groovy')) {
                files << it
            }*/
        }
        files
    }
}

