

#Jenkins Plugin.ps1
[CmdletBinding()]Param([Parameter(Mandatory=$true)][string]$JenkinsUri)

$pluginUrlBase = "/pluginManager/installNecessaryPlugins"

Write-Host "<jenkins><install plugin=""$plugin@$version"" /></jenkins>"
Write-Host "$JenkinsUri$pluginUrlBase"

# git plugin https://wiki.jenkins-ci.org/display/JENKINS/Git+Plugin
Write-Host "Installing Plugin"

$plugins = @('cloudbees-folder:4.9','nunit:0.17','msbuild:1.24','git-client:1.17.0','git:2.4.0','copyartifact:1.35.2','job-dsl:1.35',
    'email-ext:2.39.3','build-token-root:1.2','powershell:1.2','job-import-plugin:1.2','role-strategy:2.2.0','parameterized-trigger:2.27')

$pluginXml = ""

foreach($plugin in $plugins) {$a,$b = $plugin -split ':'; $pluginXml += "<install plugin=""$a@$b"" />"; Write-Host "$a $b will be installed"}

Invoke-RestMethod -Method POST -Body "<jenkins>$pluginXml</jenkins>" -Headers @{"Content-Type" = "text/xml"} -Uri "$JenkinsUri$pluginUrlBase" -OutFile plugin-response.html

# jenkins safe restart
Invoke-RestMethod -Method POST -Uri "$JenkinsUri/safeRestart"
