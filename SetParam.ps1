﻿
<#
        .SYNOPSIS
        Sets a parameter value in a SetParameters.xml file

        .PARAMETER SetParamFile
        Path to the SetParameters.xml file

        .PARAMETER ParamKey
        If specified, deploy to the specified MSDeployServiceUrl instead of what in the

        .PARAMETER Value
        If specified, use this as the IIS web application name

#>
Param(
    [Parameter(Mandatory)][string]$SetParamFile,
    [Parameter(Mandatory)][string]$ParamKey, 
    [Parameter(Mandatory)][string]$Value
)
try
{
    $key = $paramKey -replace '\+',' '
    $val = $Value -replace '\+',' '
    [xml] $xml = Get-Content -path $SetParamFile
    $xml.parameters.SelectSingleNode("setParameter[@name='$key']").value = $val
    $xml.Save($SetParamFile)
}
catch
{
    Write-Host ($_ | Format-List * -Force -ErrorAction SilentlyContinue | Out-String) -ForegroundColor Red -ErrorAction SilentlyContinue
    Write-Host ($_.Exception.StackTrace | Out-String -ErrorAction SilentlyContinue) -ForegroundColor Red -ErrorAction SilentlyContinue
    Exit 1
}