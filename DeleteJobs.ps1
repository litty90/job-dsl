﻿
#Delete jobs
$JenkinsUri = 'http://localhost:8080'
$prefix = 'aws'
foreach ($proj in @('hickory','aspen','elm','bamboo','bark','pergo'))
{
    foreach ($step in @('Start','BuildSolution','UnitTests','FluentMigrator','DeployWebsite'))
    {
        $uri = "$JenkinsUri/job/$prefix-$proj-$step/doDelete"
        Write-Host "POST $uri"
        $null = Invoke-RestMethod -Method POST  -Uri $uri
    }
    $null = Invoke-RestMethod -Method POST -Uri "$JenkinsUri/job/aws-bamboo-DeployCAP/doDelete"
    $null = Invoke-RestMethod -Method POST -Uri "$JenkinsUri/job/aws-bamboo-DeployTTS/doDelete"
    $null = Invoke-RestMethod -Method POST -Uri "$JenkinsUri/job/aws-bamboo-EFMigrate/doDelete"
    $null = Invoke-RestMethod -Method POST -Uri "$JenkinsUri/job/aws-aspen-DeployWorkflow/doDelete"
}

# jenkins safe restart
#Invoke-RestMethod -Method POST -Uri "$JenkinsUri/safeRestart"
