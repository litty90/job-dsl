powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))"
SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

choco install git -params "/GitAndUnixToolsOnPath /NoAutoCrlf"
choco install notepadplusplus
choco install dotnet3.5
:: choco install jdk8 -params "both=true"
choco install googlechrome
choco install sysinternals
choco install everything
choco install jenkins
:: choco install poshgit
choco install visualstudiocommunity2013
