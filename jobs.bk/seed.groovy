// If you want, you can define your seed job in the DSL and create it via the REST API.
// See https://github.com/sheehan/job-dsl-gradle-example#rest-api-runner

job('seed') {
    scm {
        git {
            remote {
                url('git@bitbucket.org:AlertSense/hickory.git')
                credentials('19f4b472-3a57-46b6-ad4b-0f6441867910')
            }
            branch('dev')
        }
    }

    triggers {
        scm 'H/5 * * * *'
    }
    steps {
        gradle 'clean test'
        dsl {
            external 'jobs/**/*Jobs.groovy'
            additionalClasspath 'src/main/groovy'
        }
    }
    publishers {
        archiveJunit 'create/test-results/**/*.xml'
    }
}