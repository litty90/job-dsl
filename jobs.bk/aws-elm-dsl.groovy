def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def webDeployUrl = "web1.${AWS_DOMAIN}"
def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
def dbServer = "db.${AWS_DOMAIN}"
def setParamExe = 'powershell -File C:\\Jenkins\\workspace\\gum\\provision\\aws\\deployment\\SetParam.ps1 -SetParamFile "%DEPLOY_PREF%.SetParameters.xml"'

def repoName = 'elm'
def repoBranch = 'dev'
def jobPrefix = 'aws-elm'
def dbName = 'prod-elm'
def publishProfile = 'azure-elm'
def slnDir = '%WORKSPACE%\\src'
def slnFile = 'AlertSense.Elm.sln'

def workspace = "C:\\Jenkins\\workspace\\${repoName}"
def connString = "Data Source=${dbServer},1433;Database=${dbName};Integrated Security=True"



//JOB 1 - Nuget Restore
def cmdNugetRestore = """
CD "${slnDir}"
"${slnDir}\\.nuget\\nuget.exe" restore "${slnDir}\\${slnFile}"
"""

job ("${jobPrefix}-Start") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    scm {
        git {
            remote {
                url("git@bitbucket.org:AlertSense/${repoName}.git")
                credentials("${credId}")
            }
            branch("${repoBranch}")
        }
    }
    steps {batchFile("${cmdNugetRestore}")}
    publishers {
        downstream("${jobPrefix}-BuildSolution",'SUCCESS')
    }
}

//JOB 2 - Build solution
def cmdBuildSolution = """
SET MSBUILD=${msBuildExe}
SET STD_BUILD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug
"%MSBUILD%" /t:Clean,Rebuild "${slnDir}\\${slnFile}" %STD_BUILD_ARGS%
"""

job ("${jobPrefix}-BuildSolution") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdBuildSolution}")}
    publishers {
        downstream("${jobPrefix}-UnitTests",'SUCCESS')
    }
}

//JOB 3 - Unit tests - same for all non-production stacks
def cmdUnitTests = """
SET SOLUTION_DIR=${slnDir}
SET NUNIT_EXE="%SOLUTION_DIR%\\packages\\NUnit.Runners.2.6.4\\tools\\nunit-console-x86.exe"
SET TEST_PROJ=AlertSense.MapService.Tests
SET STD_ARGS=/nologo /labels /exclude:Database,Integration /config:Debug
SET TEST_RESULTS=%SOLUTION_DIR%\\TestResults
IF NOT EXIST "%TEST_RESULTS%" mkdir "%TEST_RESULTS%"

"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\bin\\Debug\\%TEST_PROJ%.dll"
"""

job ("${jobPrefix}-UnitTests") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdUnitTests}")}
    publishers {
        downstream("${jobPrefix}-FluentMigrator",'SUCCESS')
    }
}

//JOB 4 - Fluent Migrator
//need to handle dbname and profile and server
def cmdFluentMigrator = """\
SET SOLUTION_DIR=%WORKSPACE%\\src
SET MIGRATE_EXE=%SOLUTION_DIR%\\packages\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe
SET CONFIGURATION=Debug

SET DBPROFILE=ProductionElm
SET CONN=${connString}
SET ASSEMBLY=%SOLUTION_DIR%\\DAL\\AlertSense.Elm.Data.Setup\\bin\\%CONFIGURATION%\\AlertSense.Elm.Data.Setup.dll
"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%"
IF ERRORLEVEL 1 GOTO :EOF
"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%" --profile DevelopmentElm

"""

job ("${jobPrefix}-FluentMigrator") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdFluentMigrator}")}
    publishers {
        downstream("${jobPrefix}-DeployWebsite",'SUCCESS')
    }
}

//JOB 5 - DeployWebsite

def cmdDeployWebsite = """
SET SLN_DIR=%WORKSPACE%\\src
SET CONFIGURATION=Debug
SET MSBUILD=${msBuildExe}
SET PROFILE=test-elm
SET PROJ_FILE=%SLN_DIR%\\App\\AlertSense.Elm.MappingWebsite\\AlertSense.Elm.MappingWebsite.csproj
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PROFILE%\\%PROFILE%
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=%CONFIGURATION%
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

${setParamExe} -ParamKey "ElmConnection-Web.config+Connection+String" -Value "${connString}"
type "%DEPLOY_PREF%.SetParameters.xml"

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=elm.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployWebsite") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployWebsite}")}
}
