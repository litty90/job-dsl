
def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
def setParamExe = 'powershell -File C:\\Jenkins\\workspace\\gum\\provision\\aws\\deployment\\SetParam.ps1 -SetParamFile "%DEPLOY_PREF%.SetParameters.xml"'
def dbServer = "db.${AWS_DOMAIN}"
def webDeployUrl = "web1.${AWS_DOMAIN}"

def repoBranch = 'dev'
def repoName = 'hickory'
def jobPrefix = 'aws-hickory'

def dbName = 'prod-hickory'
def siteName = 'hickory.alertsense.com'

def slnDir = '%WORKSPACE%\\src'
def slnFile = 'AlertSense.Hickory.sln'
def workspace = 'C:\\Jenkins\\workspace\\hickory'
def connString = "Data Source=${dbServer},1433;Database=${dbName};Integrated Security=True"
def publishProfile = 'azure-admin'


//JOB 1 - Nuget Restore


def cmdNugetRestore = """
CD "${slnDir}"
"${slnDir}\\.nuget\\nuget.exe" restore "${slnDir}\\${slnFile}"
"""

job ("${jobPrefix}-Start") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    scm {
        git {
            remote {
                url("git@bitbucket.org:AlertSense/${repoName}.git")
                credentials("${credId}")
            }
            branch("${repoBranch}")
        }
    }
    steps {batchFile("${cmdNugetRestore}")}
    publishers {
        downstream("${jobPrefix}-BuildSolution",'SUCCESS')
    }
}

//JOB 2 - Build solution
def cmdBuildSolution = """
SET MSBUILD=${msBuildExe}
SET STD_BUILD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug
"%MSBUILD%" /t:Clean,Rebuild "${slnDir}\\${slnFile}" %STD_BUILD_ARGS%
"""

job ("${jobPrefix}-BuildSolution") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdBuildSolution}")}
    publishers {
        downstream("${jobPrefix}-UnitTests",'SUCCESS')
    }
}

//JOB 3 - Unit tests - same for all non-production stacks
def cmdUnitTests = """
SET SOLUTION_DIR=${slnDir}
SET NUNIT_EXE=%SOLUTION_DIR%\\packages\\NUnit.Runners.2.6.4\\tools\\nunit-console-x86.exe
SET TEST_PROJ=AlertSense.Hickory.Util.Tests
SET STD_ARGS=/nologo /labels /exclude:Database,Integration /config:Debug
SET TEST_RESULTS=%SOLUTION_DIR%\\TestResults
IF NOT EXIST "%TEST_RESULTS%" mkdir "%TEST_RESULTS%"
SET BUILD_ERRORS=0

"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Data.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.AlertService.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.ContactManagementService.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\AlertSense.Hickory.Services.ContactManagement.Tests\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.MobileService.Auth.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.MobileService.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Notify.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\AlertSense.Hickory.NotifyTests\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Common.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.PublicInfoService.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Tests.TestCases
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.ServiceModel.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.Etn.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Contracts.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Hickory.Services.AdminService.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\%TEST_PROJ%.csproj"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

IF %BUILD_ERRORS%==1 EXIT /b 1
"""

job ("${jobPrefix}-UnitTests") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdUnitTests}")}
    publishers {
        downstream("${jobPrefix}-FluentMigrator",'SUCCESS')
    }
}

//JOB 4 - Fluent Migrator
//need to handle dbname and profile and server
def cmdFluentMigrator = """\
SET SOLUTION_DIR=${slnDir}
SET MIGRATE_EXE=%SOLUTION_DIR%\\packages\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe
SET CONFIGURATION=Debug

SET CONN=${connString}
SET ASSEMBLY=%SOLUTION_DIR%\\DAL\\AlertSense.Hickory.Data.Setup\\bin\\%CONFIGURATION%\\AlertSense.Hickory.Data.Setup.dll
"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%"
IF ERRORLEVEL 1 GOTO :EOF

"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%" --profile=static
IF ERRORLEVEL 1 GOTO :EOF

::"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%" --profile=DouglasR911Test
::IF ERRORLEVEL 1 GOTO :EOF

::"%MIGRATE_EXE%" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%" --profile=EasDevelopment

"""

job ("${jobPrefix}-FluentMigrator") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdFluentMigrator}")}
    publishers {
        downstream("${jobPrefix}-DeployWebsite",'SUCCESS')
    }
}

//JOB 5 - DeployWebsite

def cmdDeployWebsite = """
SET MSBUILD=${msBuildExe}
SET SLN_DIR=${slnDir}
SET PROFILE=${publishProfile}
SET PROJ_FILE=%SLN_DIR%\\App\\AlertSense.Hickory.AdminWebsite\\AlertSense.Hickory.AdminWebsite.csproj
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=Debug
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%SLN_DIR%\\WebPackages\\%PROFILE%\\%PROFILE%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

SET DEPLOY_URL=${webDeployUrl}
SET OTHER_ARGS=-allowUntrusted -enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='${siteName}'"

SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PROFILE%\\%PROFILE%

${setParamExe} -ParamKey "HickoryConnection-Web.config+Connection+String" -Value "${connString}"
${setParamExe} -ParamKey  "HickoryLoggingConnection-Web.config+Connection+String" -Value "${connString}"

type "%DEPLOY_PREF%.SetParameters.xml"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL%  %WEB_CREDS% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployWebsite") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployWebsite}")}
    publishers {
        downstream("${jobPrefix}-DeployPublicWebsite",'SUCCESS')
    }
}

//JOB 6 - Deploy Public Website

def cmdDeployPublicWebsite = """
SET MSBUILD=${msBuildExe}
SET SLN_DIR=${slnDir}
SET PUBPROFILE=dev-public
SET PKGPROFILE=aws-public
SET PROJ_FILE=%SLN_DIR%\\App\\AlertSense.Hickory.PublicWeb\\AlertSense.Hickory.PublicWeb.csproj
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=Debug
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PKGPROFILE%\\%PKGPROFILE%

SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

SET DEPLOY_URL=${webDeployUrl}
SET OTHER_ARGS=-allowUntrusted -enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='public.alertsense.com'"

${setParamExe} -ParamKey "HickoryConnection-Web.config+Connection+String" -Value "${connString}"
type "%DEPLOY_PREF%.SetParameters.xml"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL%  %WEB_CREDS% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployPublicWebsite") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployPublicWebsite}")}
}
