def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def repoName = 'aspen'
def repoBranch = 'awsprofile'
def jobPrefix = 'aws-aspen'
def dbName = 'aspen'
def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
def webDeployUrl = "web1.${AWS_DOMAIN}"
def slnDir = '%WORKSPACE%'
def slnFile = 'AlertSense.Aspen.sln'
def workspace = 'C:\\Jenkins\\workspace\\aspen'
def connString = "Data Source=db.${AWS_DOMAIN},1433;Database=${dbName};Integrated Security=True"
def workflowConnString = "Data Source=db.${AWS_DOMAIN},1433;Database=production-workflowinstancestore;Integrated Security=True"
def serviceConnString = "Data Source=db.${AWS_DOMAIN},1433;Database=aspen-monitoring;Integrated Security=True"
def publishProfile = 'aws-aspen'
def setParamExe = 'powershell -File C:\\Jenkins\\workspace\\gum\\provision\\aws\\deployment\\SetParam.ps1 -SetParamFile "%DEPLOY_PREF%.SetParameters.xml"'
def migrateExe = '%WORKSPACE%\\packages\\FluentMigrator.1.3.0.0\\tools\\Migrate.exe'

//JOB 1 - Nuget Restore
def cmdNugetRestore = """
CD "${slnDir}"
"${slnDir}\\.nuget\\nuget.exe" restore "${slnDir}\\${slnFile}"
"""

job ("${jobPrefix}-Start") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    scm {
        git {
            remote {
                url("git@bitbucket.org:AlertSense/${repoName}.git")
                credentials("${credId}")
            }
            branch("${repoBranch}")
        }
    }
    steps {batchFile("${cmdNugetRestore}")}
    publishers {
        downstream("${jobPrefix}-BuildSolution",'SUCCESS')
    }
}

//JOB 2 - Build solution
def cmdBuildSolution = """
SET MSBUILD=${msBuildExe}
SET STD_BUILD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug
"%MSBUILD%" /t:Clean,Rebuild "${slnDir}\\${slnFile}" %STD_BUILD_ARGS%
"""

job ("${jobPrefix}-BuildSolution") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdBuildSolution}")}
    publishers {
        downstream("${jobPrefix}-UnitTests",'SUCCESS')
    }
}

//JOB 3 - Unit tests - same for all non-production stacks
def cmdUnitTests = '''
SET SOLUTION_DIR=%WORKSPACE%
SET NUNIT_EXE=C:\\Program Files (x86)\\NUnit 2.6.4\\bin\\nunit-console-x86.exe
SET STD_ARGS=/nologo /labels /exclude:Database,Integration /config:Debug /domain=multiple
SET TEST_RESULTS=%SOLUTION_DIR%\\TestResults
IF NOT EXIST "%TEST_RESULTS%" mkdir "%TEST_RESULTS%"
SET BUILD_ERRORS=0

SET TEST_PROJ=AlertSense.Aspen.CallFireProvider.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.CallFireProvider.Tests\\bin\\Debug\\AlertSense.Aspen.CallFireProvider.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.CampaignXml.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.CampaignXml.Tests\\bin\\Debug\\AlertSense.Aspen.CampaignXml.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.Common.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.Common.Tests\\bin\\Debug\\AlertSense.Aspen.Common.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

::SET TEST_PROJ=AlertSense.Aspen.Data.Tests
::"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.Data.Tests\\bin\\Debug\\AlertSense.Aspen.Data.Tests.dll"
::IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.VoiceModule.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.Modules.VoiceModule.Tests\\bin\\Debug\\AlertSense.Aspen.VoiceModule.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.PlivoProvider.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.PlivoProvider.Tests\\bin\\Debug\\AlertSense.Aspen.PlivoProvider.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.Service.Test
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.Tests\\bin\\Debug\\AlertSense.Aspen.Service.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

::SET TEST_PROJ=AlertSense.Aspen.TextModule.Tests
::"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.TextModule.Tests\\bin\\Debug\\AlertSense.Aspen.TextModule.Tests.dll"
::IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.TwilioProvider.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.TwilioProvider.Tests\\bin\\Debug\\AlertSense.Aspen.TwilioProvider.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.VoiceModule.Workflow.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.VoiceModule.Workflow.Tests\\bin\\Debug\\AlertSense.Aspen.VoiceModule.Workflow.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.VoxeoProvider.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.VoxeoProvider.Tests\\bin\\Debug\\AlertSense.Aspen.VoxeoProvider.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.WindowsWorkflow.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\AlertSense.Aspen.WindowsWorkflow.Tests\\bin\\Debug\\AlertSense.Aspen.WindowsWorkflow.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Aspen.Workflow.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\AlertSense.Aspen.Workflow.Tests\\bin\\Debug\\AlertSense.Aspen.Workflow.Tests.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

IF %BUILD_ERRORS%==1 EXIT /b 1

'''

job ("${jobPrefix}-UnitTests") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdUnitTests}")}
    publishers {
        downstream("${jobPrefix}-FluentMigrator",'SUCCESS')
    }
}

//JOB 4 - Fluent Migrator
//need to handle dbname and profile and server
def cmdFluentMigrator = """\
SET DBPROFILE=DevelopmentElm
SET CONN=${connString}
SET ASSEMBLY=%WORKSPACE%\\AlertSense.Aspen.Data\\bin\\Debug\\AlertSense.Aspen.Data.dll
"${migrateExe}" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="%CONN%" --profile=%DBPROFILE%
"""

job ("${jobPrefix}-FluentMigrator") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdFluentMigrator}")}
    publishers {
        downstream("${jobPrefix}-FluentMigratorWorkflow",'SUCCESS')
    }
}

def cmdFluentMigratorWorkflow = """\
SET ASSEMBLY=%WORKSPACE%\\AlertSense.Aspen.Workflow.Data.Instance\\bin\\Debug\\AlertSense.Aspen.Workflow.Data.Instance.dll
"${migrateExe}" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="${workflowConnString}"
IF %ERRORLEVEL% NEQ 0 GOTO :EOF

SET ASSEMBLY=%WORKSPACE%\\AlertSense.Aspen.Workflow.Data.Tracking\\bin\\Debug\\AlertSense.Aspen.Workflow.Data.Tracking.dll
"${migrateExe}" --provider sqlserver2012 --assembly "%ASSEMBLY%" --task migrate --verbose==true --connectionString="${serviceConnString}"
"""

job ("${jobPrefix}-FluentMigratorWorkflow") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdFluentMigratorWorkflow}")}
    publishers {
        downstream("${jobPrefix}-DeployWebsite",'SUCCESS')
    }
}

//JOB 5 - DeployWebsite

def cmdDeployWebsite = """
SET MSBUILD=${msBuildExe}
SET SLN_DIR=%WORKSPACE%
SET PROFILE=${publishProfile}
SET PROJ_FILE=%SLN_DIR%\\AlertSense.Aspen\\AlertSense.Aspen.csproj
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PROFILE%\\%PROFILE%
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=Debug
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

::${setParamExe} -ParamKey "AlertSense.Aspen-Web.config+Connection+String" -Value "${connString}"
::type "%DEPLOY_PREF%.SetParameters.xml"

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=aspen.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployWebsite") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployWebsite}")}
    publishers {
        downstream("${jobPrefix}-DeployWorkflow",'SUCCESS')
    }
}

//JOB 6 DeployWorkflow
def cmdDeployWorkflow = """
SET MSBUILD=${msBuildExe}
SET SLN_DIR=%WORKSPACE%
SET PROFILE=${publishProfile}
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\aspen-workflow\\aspen-workflow

SET PROJ_FILE=%SLN_DIR%\\AlertSense.Aspen.Workflow\\AlertSense.Aspen.Workflow.csproj
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=Staging
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

::${setParamExe} -ParamKey "WorkflowInstanceStore-Web.config+Connection+String" -Value "${workflowConnString}"
::${setParamExe} -ParamKey "ServiceMonitoring-Web.config+Connection+String" -Value "${serviceConnString}"
::type "%DEPLOY_PREF%.SetParameters.xml"

IF ERRORLEVEL 1 GOTO :EOF

SET SITENAME=aspen-workflow.alertsense.com
SET OTHER_ARGS=-allowUntrusted -enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"
"%DEPLOY_PREF%.deploy.cmd" /Y /m:${webDeployUrl}  %WEB_CREDS%  %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployWorkflow") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployWorkflow}")}
}
