import utilities.AlertSense

def project = 'bark'
def environment = 'dev'
def branchName = 'dev'
def repoUri = "https://josiahpeters@bitbucket.org/AlertSense/${project}.git"

dev deployServer = "192.168.50.98"

//gitHubCredentials = jm.getCredentialsId('my-credentials-description')
folder(environment)

def buildname = "$environment/${project}-create"
def testname = "$environment/${project}-test"
def deployname = "$environment/${project}-deploy"


// Build Job
job(buildname) {
	scm {
		git {
            remote {
				name("BitBucket")
                url(repoUri)
				credentials('ec6e93bf-8e81-4ff0-87d9-6af4ae3aa772')
            }
			clean(true)
			branch(branchName)
        }
	}
	configure AlertSense.MSBuild("Msbuild2015", "src/AlertSense.Bark.sln", "")
	configure AlertSense.MSBuild("Msbuild2015", "src/AlertSense.Bark.FeedWebsite/AlertSense.Bark.FeedWebsite.csproj","/p:RunOctoPack=true")
	configure AlertSense.MSBuild("Msbuild2015", "src/App/AlertSense.Bark.AshWebsite/AlertSense.Bark.AshWebsite.csproj","/p:RunOctoPack=true") 	
	publishers {
        archiveArtifacts {	
			pattern('**/*.*')
			onlyIfSuccessful(true)
		}
		downstream(testname,'SUCCESS')
    }	
}

// Testing Job
job(testname) {
	steps {
        copyArtifacts(buildname) {
            includePatterns('**/*.*')
            buildSelector {
                latestSuccessful(true)
            }
        }
    }	
	configure AlertSense.PowerShell('''\$runner = "C:\\Program Files (x86)\\NUnit 2.6.4\\bin\\nunit-console.exe" 
\$args = @("/labels","/timeout=600","/framework=4.5","/process=Multiple","") 
\$projects = Resolve-Path src\\Tests\\*\\*Tests*.csproj 
Write-Host \$projects \$i = 0 
foreach(\$project in \$projects) { 
\$argflat = [string]::Join(" ", \$args) 
Write-Host "& \$runner \$project \$argflat /xml=test-\$i.xml" 
& \$runner \$project @args "/xml=test-\$i.xml" 
\$i++ 
}''')
	publishers {
		configure AlertSense.NunitTest("**/test-*.xml")
	}	
}

// Deployment Job
job(deployname) {
	steps {
        copyArtifacts(buildname) {
            includePatterns('**/*.*')
            buildSelector {
                latestSuccessful(true)
            }
        }
    }	
	configure AlertSense.PowerShell(".\$workspace\\create\\jenkins\\deploy-package.ps1 -PackageName AlertSense.Bark.FeedWebsite -NugetPath $workspace\\src\\.nuget -WebSiteName dev-msdeploy/feed -ServerName 192.168.50.98 -Force")
	//configure AlertSense.Batch("\$workspace\\deploy\\ash-web.deploy.cmd /y /m:$deployServer -enableRule:AppOffline")
    //configure AlertSense.Batch("\$workspace\\deploy\\feed-web.deploy.cmd /y /m:$deployServer -enableRule:AppOffline")
	//publishers {
	//	configure AlertSense.NunitTest("**/test-*.xml")
	//}	
}