def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
def dbServer = "db.${AWS_DOMAIN}"
def webDeployUrl = "web1.${AWS_DOMAIN}"
def setParamExe = 'powershell -File C:\\Jenkins\\workspace\\gum\\provision\\aws\\deployment\\SetParam.ps1 -SetParamFile "%DEPLOY_PREF%.SetParameters.xml"'

def repoName = 'bamboo'
def repoBranch = 'dev'

def dbName = 'AlertSenseCap_Generated'
def slnFile = 'Alertsense CAP.sln'

def siteName = 'alertsense.bamboo.com'
def siteName2 = 'tts.alertsense.com'

def jobPrefix = "aws-${repoName}"
def slnDir = '%WORKSPACE%\\src'

def workspace = "C:\\Jenkins\\workspace\\${repoName}"
def connString = "Data Source=${dbServer},1433;Database=AlertSenseCap_Generated;Integrated Security=True;User Instance=false;MultipleActiveResultSets=true"


//JOB 1 - Nuget Restore
def cmdNugetRestore = """
CD "${slnDir}"
"${slnDir}\\.nuget\\nuget.exe" restore "${slnDir}\\${slnFile}"
"""

job ("${jobPrefix}-Start") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    scm {
        git {
            remote {
                url("git@bitbucket.org:AlertSense/${repoName}.git")
                credentials("${credId}")
            }
            branch("${repoBranch}")
        }
    }
    steps {batchFile("${cmdNugetRestore}")}
    publishers {
        downstream("${jobPrefix}-BuildSolution",'SUCCESS')
    }
}

//JOB 2 - Build solution
def cmdBuildSolution = """
SET MSBUILD=${msBuildExe}
SET STD_BUILD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:1 /nologo /v:n /p:Configuration=Debug
"%MSBUILD%" /t:Clean,Rebuild "${slnDir}\\${slnFile}" %STD_BUILD_ARGS%
"""

job ("${jobPrefix}-BuildSolution") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdBuildSolution}")}
    publishers {
        downstream("${jobPrefix}-EFMigrate",'SUCCESS')
    }
}

//JOB3 - Entity Framework DB Migrate
def cmdEFMigrate = """
SET MIGRATE_EXE=%workspace%\\src\\packages\\EntityFramework.4.3.1\\tools\\migrate.exe
"%MIGRATE_EXE%" AlertSense.Core /StartUpDirectory="%workspace%\\src\\Libraries\\AlertSense.Core\\bin\\Debug" /connectionString="${connString}" /connectionProviderName="System.Data.SqlClient"
"""

job ("${jobPrefix}-EFMigrate") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdEFMigrate}")}
    publishers {
        downstream("${jobPrefix}-UnitTests",'SUCCESS')
    }
}

//JOB4 - Unit tests
def cmdUnitTests = """
SET ERRORS_FOUND=0
SET NUNIT_EXE=%WORKSPACE%\\src\\packages\\NUnit.Runners.2.6.4\\tools\\nunit-console-x86.exe
"%NUNIT_EXE%"  /nologo /labels /exclude:Database,Integration "%workspace%\\src\\tests\\AlertSense.CAP.Test\\AlertSense.CAP.Test.csproj"
IF %errorlevel% NEQ 0 SET ERRORS_FOUND=0

"%NUNIT_EXE%" /nologo /labels /exclude:Database,Integration "%workspace%\\src\\AlertSense.Bamboo.Tts.Tests\\AlertSense.Bamboo.Tts.Tests.csproj"
IF %errorlevel% NEQ 0 SET ERRORS_FOUND=0

"%NUNIT_EXE%" /nologo /labels /exclude:Database,Integration "%workspace%\\src\\AlertSense.Bamboo.Core.Tests\\AlertSense.Bamboo.Core.Tests.csproj"
IF %errorlevel% NEQ 0 SET ERRORS_FOUND=0
:: IF %ERRORS_FOUND%==0 Exit /b 1
"""

job ("${jobPrefix}-UnitTests") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdUnitTests}")}
    publishers {
        downstream("${jobPrefix}-DeployTTS",'SUCCESS')
    }
}

//JOB5 - Deploy TTS
def cmdDeployTTS = """
SET MSBUILD=${msBuildExe}
SET PROFILE-azure-tts
SET DEPLOY_PREF=${slnDir}\\WebPackages\\aws-tts\\aws-tts
SET PROJ_FILE=%workspace%\\src\\Applications\\AlertSense.Web.TtsService\\AlertSense.Web.TtsService.csproj
SET CAP_CONN=${connString}

"%MSBUILD%" /t:Package /p:Configuration=Debug /p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip" "%PROJ_FILE%"
IF %errorlevel% NEQ 0 GOTO :EOF

${setParamExe} -ParamKey "AlertSenseCap-Web.config+Connection+String" -Value "%CAP_CONN%"
type "%DEPLOY_PREF%.SetParameters.xml"

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=tts.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployTTS") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployTTS}")}
    publishers {
        downstream("${jobPrefix}-DeployCAP",'SUCCESS')
    }
}

//JOB6 - Deploy CAP
def cmdDeployCAP = """
SET MSBUILD=${msBuildExe}
SET PROFILE=dev-cap
SET DEPLOY_PREF=${slnDir}\\WebPackages\\aws-cap\\aws-cap
SET PROJ_FILE=%workspace%\\src\\Applications\\AlertSense.Website\\AlertSense.Website.csproj
SET CAP_CONN=${connString}

"%MSBUILD%" /t:Package /p:Configuration=Debug /p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip" "%PROJ_FILE%"
IF %errorlevel% NEQ 0 GOTO :EOF

${setParamExe} -ParamKey "AlertSenseCap-Web.config+Connection+String" -Value "%CAP_CONN%"
type "%DEPLOY_PREF%.SetParameters.xml"

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=bamboo.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployCAP") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployCAP}")}
}
