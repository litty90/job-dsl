def dbServer = "db.${AWS_DOMAIN}"
def webDeployUrl = "web1.${AWS_DOMAIN}"
def msBuildExe = 'C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe'
def credId = '19f4b472-3a57-46b6-ad4b-0f6441867910'
def setParamExe = 'powershell -File C:\\Jenkins\\workspace\\gum\\provision\\aws\\deployment\\SetParam.ps1 -SetParamFile "%DEPLOY_PREF%.SetParameters.xml"'

def repoName = 'bark'
def repoBranch = 'dev'
def jobPrefix = 'aws-bark'

def slnDir = '%WORKSPACE%\\src'
def slnFile = "AlertSense.${repoName}.sln"
def workspace = "C:\\Jenkins\\workspace\\${repoName}"
def publishProfile = 'dev-bark'


//JOB 1 - Nuget Restore
def cmdNugetRestore = """
CD "${slnDir}"
"${slnDir}\\.nuget\\nuget.exe" restore "${slnDir}\\${slnFile}"
"""

job ("${jobPrefix}-Start") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    scm {
        git {
            remote {
                url("git@bitbucket.org:AlertSense/${repoName}.git")
                credentials("${credId}")
            }
            clean(true)
            branch("${repoBranch}")
        }
    }
    steps {batchFile("${cmdNugetRestore}")}
    publishers {
        downstream("${jobPrefix}-BuildSolution",'SUCCESS')
    }
}

//JOB 2 - Build solution
def cmdBuildSolution = """
SET MSBUILD=${msBuildExe}
SET STD_BUILD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m /nologo /v:n /p:Configuration=Debug
"%MSBUILD%" /t:Clean,Rebuild "${slnDir}\\${slnFile}" %STD_BUILD_ARGS%
"""

job ("${jobPrefix}-BuildSolution") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdBuildSolution}")}
    publishers {
        downstream("${jobPrefix}-UnitTests",'SUCCESS')
    }
}

//JOB 3 - Unit tests - same for all non-production stacks
def cmdUnitTests = """
SET SOLUTION_DIR=${slnDir}
SET NUNIT_EXE="%SOLUTION_DIR%\\packages\\NUnit.Runners.2.6.4\\tools\\nunit-console-x86.exe"
SET STD_ARGS=/nologo /labels /exclude:Database,Integration /config:Debug
SET TEST_RESULTS=%SOLUTION_DIR%\\TestResults
IF NOT EXIST "%TEST_RESULTS%" mkdir "%TEST_RESULTS%"

SET BUILD_ERRORS=0

SET TEST_PROJ=AlertSense.Bark.Data.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\bin\\Debug\\%TEST_PROJ%.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Bark.Services.Feed.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\bin\\Debug\\%TEST_PROJ%.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

SET TEST_PROJ=AlertSense.Bark.Services.Polling.Tests
"%NUNIT_EXE%" %STD_ARGS% /result="%TEST_RESULTS%\\%TEST_PROJ%.xml" "%SOLUTION_DIR%\\Tests\\%TEST_PROJ%\\bin\\Debug\\%TEST_PROJ%.dll"
IF %ERRORLEVEL% NEQ 0 SET BUILD_ERRORS=1

IF %BUILD_ERRORS%==1 EXIT /b 1
"""

job ("${jobPrefix}-UnitTests") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdUnitTests}")}
    publishers {
        downstream("${jobPrefix}-DeployBarkFeedWebsite",'SUCCESS')
    }
}


//JOB 5 - DeployFeedWebsite

def cmdDeployFeedWebsite = """
SET SLN_DIR=%WORKSPACE%\\src
SET CONFIGURATION=Debug
SET MSBUILD=${msBuildExe}
SET PROFILE=dev-bark
SET PKGPROFILE=bark-feed
SET PROJ_FILE=%SLN_DIR%\\AlertSense.Bark.FeedWebsite\\AlertSense.Bark.FeedWebsite.csproj
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PKGPROFILE%\\%PKGPROFILE%
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=%CONFIGURATION%
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=bark.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

job ("${jobPrefix}-DeployBarkFeedWebsite") {
    customWorkspace("${workspace}")
    logRotator(60,-1,-1,-1)
    steps {batchFile("${cmdDeployFeedWebsite}")}
}

//JOB  - DeployBarkAshWebsite

def cmdDeployAshWebsite = """
SET SLN_DIR=%WORKSPACE%\\src
SET CONFIGURATION=Debug
SET MSBUILD=${msBuildExe}
SET PROFILE=dev-bark
SET PKGPROFILE=bark-ash
SET PROJ_FILE=%SLN_DIR%\\AlertSense.Bark.FeedWebsite\\AlertSense.Bark.FeedWebsite.csproj
SET DEPLOY_PREF=%SLN_DIR%\\WebPackages\\%PKGPROFILE%\\%PKGPROFILE%
SET BLD_ARGS=/p:VisualStudioVersion=12.0 /tv:4.0 /m:4 /v:n /nologo /p:Configuration=%CONFIGURATION%
SET PUB_ARGS=/p:PublishProfile=%PROFILE% /p:PackageLocation="%DEPLOY_PREF%.zip"

"%MSBUILD%"  "%PROJ_FILE%" /t:Package %BLD_ARGS% %PUB_ARGS%
IF ERRORLEVEL 1 GOTO :EOF

SET DEPLOY_URL=${webDeployUrl}
SET SITENAME=bark-feed.alertsense.com
SET OTHER_ARGS=-enableRule:AppOffline "-setParam:name='IIS Web Application Name',value='%SITENAME%'"

"%DEPLOY_PREF%.deploy.cmd" /Y /m:%DEPLOY_URL% %OTHER_ARGS%
"""

//job ("${jobPrefix}-DeployBarkAshWebsite") {
//    customWorkspace("${workspace}")
//    logRotator(60,-1,-1,-1)
//    steps {batchFile("${cmdDeployAshWebsite}")}
//}
